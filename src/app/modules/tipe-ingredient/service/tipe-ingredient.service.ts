import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipeIngredientService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoTiposIngredientes() {
    return this.http.get(url + '/tipoIngrediente/listar')
  }

  crearTipoIngrediente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/tipoIngrediente/crear', data, option);
  }

  buscarIngredienteStock(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/tipoIngrediente/buscar/ingrediente', data, option);
  }

  obtenerListadoMarcas() {
    return this.http.get(url + '/marca/listar')
  }
  
  valorizarIngredientes() {
    return this.http.get(url + '/tipoIngrediente/valorizar/ingrediente')
  }

  valorizarFormatosProductos() {
    return this.http.get(url + '/producto/valorizar/formatos')
  }

  eliminarTipoIngrediente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/tipoIngrediente/eliminar', data, option);
  }

  editarTipoIngrediente(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/tipoIngrediente/editar', data, option);
  }

  crearIngrediente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/ingrediente/crear', data, option);
  }

  actualizarIngrediente(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/tipoIngrediente/actualizar', data, option);
  }

  editarIngrediente(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/ingrediente/editar', data, option);
  }

  eliminarIngrediente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/ingrediente/eliminar', data, option);
  }

  buscarFormatosPorIngrediente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/formato/ingrediente/buscar/formatos', data, option);
  }
}
