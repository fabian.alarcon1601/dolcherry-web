import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from '../../service/home.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  categorias: any;
  productos: any;

  constructor(private homeService: HomeService, private router: Router) { }

  ngOnInit(): void {
    this.obtenerCategorias()
    this.obtenerProductos()
  }


  obtenerProductos() {
    this.homeService.listarProductos().subscribe((resp: any) => {
      console.log(resp.data);
      this.productos = resp.data
    })
  }

  obtenerCategorias() {
    this.homeService.listarCategorias().subscribe((resp: any) => {
      this.categorias = resp.data
    })
  }

  obtenerImagen(item) {
    let resultado = item.replaceAll('\\', "/")
    let ruta = 'http://localhost:3001/'
    return ruta + resultado;
  }

  sendProduct(item) {
    let format = item.nombre.replaceAll(' ', '-').toLowerCase();
    this.router.navigate(['/view/product', format], {
      state: {
        item
      }
    });
  }
}
