import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipeIngredientComponent } from './components/tipe-ingredient/tipe-ingredient.component';
import { StockComponent } from './components/stock/stock.component';

const routes: Routes = [
  { path: '', component: TipeIngredientComponent },
  { path:'stock/:ingredient', component:StockComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class TipeIngredientRoutingModule { }
