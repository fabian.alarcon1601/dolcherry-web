import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from '../translateDataTable/language';
import { DashboardService } from './services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  pedidosMes: any;
  ingredientesVencidosMesCount: any = 0;
  ingredientesVencidosMes: any;
  ingredientesPorVencer: any;
  ingredientesPorVencerCount: any;
  vencidoMesAnterior: any = 0;
  trafficChart = [];
  mesAnterior1:any;
  mesAnterior2:any;
  contadorMes1:any;
  contadorMes2:any;
  visitSaleBarLabel = [];
  visitSaleChartDat = [];

  toggleProBanner(event) {
    console.log("123");
    event.preventDefault();
    document.querySelector('body').classList.toggle('removeProbanner');
  }

  constructor(private dashboardService: DashboardService) { }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerPedidosDelMes();
    this.obtenerIngredientesPorVencer();
    this.obtenerIngredientesVencidosMesAnterior();
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerPedidosDelMes() {
    this.dashboardService.obtenerPedidosMes().subscribe((resp1: any) => {
      this.pedidosMes = resp1.data;
      this.dashboardService.obtenerPedidosDosMesesAnteriores().subscribe((resp:any)=>{
        this.setTrafficBarData(resp.data1, resp.data2 ,resp1.data );
        this.visitSaleBarLabels(resp.mes1, resp.mes2);
      })
    })
  }


  obtenerIngredientesVencidosMes() {
    this.dashboardService.obtenerIngredientesVencidosMes().subscribe((resp: any) => {
      this.ingredientesVencidosMesCount = resp.count;
      this.ingredientesVencidosMes = resp.data;
      this.rerender();
    })
  }

  obtenerIngredientesVencidosMesAnterior() {
    this.dashboardService.obtenerIngredientesVencidosMesAnterior().subscribe((resp: any) => {
      this.vencidoMesAnterior = resp.data;
      this.dashboardService.obtenerIngredientesVencidosMes().subscribe((resp2: any) => {
        this.ingredientesVencidosMesCount = resp2.count;
        this.ingredientesVencidosMes = resp2.data;

        this.setTrafficChartData(resp.data, resp2.count)
      })
    })
  }

  obtenerIngredientesPorVencer() {
    this.dashboardService.obtenerIngredientesPorVencer().subscribe((resp: any) => {
      this.ingredientesPorVencerCount = resp.count;
      this.ingredientesPorVencer = resp.data;
      this.rerender();
    })
  }

  setTrafficChartData(dato1 , dato2){
    let trafficChartData2 = [
      {
        data: [dato1, dato2],
      }
    ];
    this.trafficChart = trafficChartData2
  }
  //datos grafico de pie 

  setTrafficBarData(count1, count2, count3 ){
    let visitSaleChartData = [{
      label: 'Pedidos',
      data: [count2, count1, count3],
      borderWidth: 1,
      fill: false,
    }];
    this.visitSaleChartDat = visitSaleChartData
  } 

  visitSaleBarLabels(mes1, mes2){
    let visitSaleChartLabels = [mes2,mes1,'mes actual'];
    this.visitSaleBarLabel = visitSaleChartLabels
  }
 

  trafficChartLabels = ["Mes anterior", "Mes actual"];

  trafficChartOptions = {
    responsive: true,
    animation: {
      animateScale: true,
      animateRotate: true
    },
    legend: false,
  };

  trafficChartColors = [
    {
      backgroundColor: [
        'rgba(177, 148, 250, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(132, 217, 210, 1)'
      ],
      borderColor: [
        'rgba(177, 148, 250, .2)',
        'rgba(254, 112, 150, .2)',
        'rgba(132, 217, 210, .2)'
      ]
    }
  ];

  // datos grafico de pie

  date: Date = new Date();

  barChartOptions = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };


  visitSaleChartOptions = {
    responsive: true,
    legend: false,
    scales: {
      yAxes: [{
        ticks: {
          display: true,
          min: 0,
          stepSize: 10,
          max: 80,
          
        },
        gridLines: {
          drawBorder: false,
          color: 'rgba(235,237,242,1)',
          zeroLineColor: 'rgba(235,237,242,1)'
        }
      }],
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false,
          color: 'rgba(0,0,0,1)',
          zeroLineColor: 'rgba(235,237,242,1)'
        },
        ticks: {
          padding: 20,
          fontColor: "#9c9fa6",
          autoSkip: true,
        },
        categoryPercentage: 0.4,
        barPercentage: 0.4
      }]
    }
  };

  visitSaleChartColors = [
    {
      backgroundColor: [
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
      ],
      borderColor: [
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
        'rgba(154, 85, 255, 1)',
      ]
    },
    {
      backgroundColor: [
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
      ],
      borderColor: [
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(254, 112, 150, 1)',
      ]
    },
    {
      backgroundColor: [
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
      ],
      borderColor: [
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
        'rgba(177, 148, 250, 1)',
      ]
    },
  ];



}
