import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoProductos() {
    return this.http.get(url + '/producto/listar')
  }

  obtenerListadoCategorias() {
    return this.http.get(url + '/categoria/listar')
  }

  obtenerListadoTipoProducto() {
    return this.http.get(url + '/tipo/producto/listar')
  }

  obtenerListadoTiposIngredientes() {
    return this.http.get(url + '/tipoIngrediente/listar')
  }

  crearProducto(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/crear', data, option);
  }

  editarProducto(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/editar', data, option);
  }
}
