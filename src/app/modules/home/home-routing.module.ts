import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LocationComponent } from './components/location/location.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactComponent } from './components/contact/contact.component';
import { ViewProductComponent } from './components/view-product/view-product.component';

const routes: Routes = [
  {path: '', component:HomepageComponent},
  {path: 'location', component:LocationComponent},
  {path: 'about', component:AboutUsComponent},
  {path: 'contact', component:ContactComponent},
  {path: 'view/product/:format', component:ViewProductComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
  
})
export class HomeRoutingModule { }
