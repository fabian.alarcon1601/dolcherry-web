import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  helper = new JwtHelperService()
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  signIn(data) {
    const option = { headers: this.headers };
    return this.http.post(url + '/usuario/login', data, option);
  }

  saveToken(token) {
    localStorage.setItem('token', token);
  }

  getUser() {
    let token = localStorage.getItem('token');
    if (token != null) {
      let usuario = this.helper.decodeToken(token).usuario;
      return usuario;
    } else {
      return false;
    }
  }

  getRol() {
    let token = localStorage.getItem('token');
    let rol = this.helper.decodeToken(token).usuario.rol;
    return rol;
  }

  logOut() {
    localStorage.removeItem('token')
  }

  getCor(data) {
    const option = { headers: this.headers };
    return this.http.post(url + '/usuario/buscar/correo', data, option);
  }
}
