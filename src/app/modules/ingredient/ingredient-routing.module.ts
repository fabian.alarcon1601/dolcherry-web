import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngredientComponent } from './components/ingredient/ingredient.component';
import { StockComponent } from './components/stock/stock.component';

const routes: Routes = [
  {path: '', component: IngredientComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [StockComponent]
})
export class IngredientRoutingModule { }
