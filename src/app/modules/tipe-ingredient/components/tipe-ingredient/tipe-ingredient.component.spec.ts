import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipeIngredientComponent } from './tipe-ingredient.component';

describe('TipeIngredientComponent', () => {
  let component: TipeIngredientComponent;
  let fixture: ComponentFixture<TipeIngredientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipeIngredientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipeIngredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
