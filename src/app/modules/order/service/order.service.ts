import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoOrdenes() {
    return this.http.get(url + '/pedido/listar')
  }

  obtenerListadoProductos() {
    return this.http.get(url + '/producto/listar')
  }

  obtenerListadoOrdenesHoy() {
    return this.http.get(url + '/pedido/listar/hoy')
  }

  obtenerListadoOrdenesManiana() {
    return this.http.get(url + '/pedido/listar/maniana')
  }

  obtenerListadoClientes(){
    return this.http.get(url + '/cliente/listar') 
  }

  crearOrder(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/pedido/crear', data, option);
  }

  obtenerFormatosPorPedido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/buscar/detalle', data, option)
  }

  actualizarRecibido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/actualizar/recibido', data, option)
  }

  cambiarEstadoEntregado(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/entregar/pedido', data, option)
  }

  cancelarEntregaPedido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/cancelar/recibido', data, option)
  }

  editarPedido(data) {
    const option = {headers:this.headers};
    return this.http.put(url + '/pedido/editar', data, option)
  }

  editarFormatosDePedido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/editar/formato', data, option)
  }

  crearFormatoDePedido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/crear/formato', data, option)
  }

  eliminarFormatoDePedido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/formato/pedido/eliminar', data, option)
  }

  eliminarPedido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/eliminar', data, option)
  }

  eliminarFormatosDeUnPedido(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/formato/pedido/eliminar/pedido', data, option)
  }

  finalizarPedido(data) {
    const option = {headers:this.headers};
    return this.http.put(url + '/pedido/finalizar', data, option)
  }

  pasarPreparacion(data) {
    const option = {headers:this.headers};
    return this.http.post(url + '/pedido/preparar', data, option)
  }
}