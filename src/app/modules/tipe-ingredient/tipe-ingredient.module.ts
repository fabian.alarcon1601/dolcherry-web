import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipeIngredientRoutingModule } from './tipe-ingredient-routing.module';
import { DataTablesModule } from 'angular-datatables';
import { TipeIngredientComponent } from './components/tipe-ingredient/tipe-ingredient.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StockComponent } from './components/stock/stock.component';


@NgModule({
  declarations: [TipeIngredientComponent,StockComponent],
  imports: [
    CommonModule,
    TipeIngredientRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class TipeIngredientModule { }
