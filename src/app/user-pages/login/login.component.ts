import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  singInUser = this.formBuilder.group({
    correo: new FormControl('', [Validators.required ,Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    password: new FormControl('', Validators.required)
  });


  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
  }

  sigIn(form) {
    let data = {
      correo: form.correo,
      password: form.password
    }
    this.userService.signIn(data).subscribe(async (resp: any) => {
      if (resp.code === 200) {
        if (resp.data.rol.operativo === true) {
          if (resp.data.operativo === true) {
            this.userService.saveToken(resp.token);
            this.router.navigate(['/dashboard']);
            this.Toast.fire({
              icon: 'success',
              title: `Bienvenido a dolcherry ${resp.data.nombre} ${resp.data.apellido_paterno}`
            })
          } else {
            Swal.fire({
              icon: 'error',
              title: `El usuario se encuentra actualmente desactivado`,
              showConfirmButton: false,
            })
          }
        }else {
          Swal.fire({
            icon: 'error',
            title: `El rol del usuario se encuentra actualmente desactivado`,
            showConfirmButton: false,
          })
        }
      } else {
        Swal.fire({
          icon: 'error',
          title: `${resp.message}`,
          showConfirmButton: false,
          timer: 2000
        })
      }
    })
  }


  reset() {
    this.router.navigate(['/user-pages/reset']);
  }
}
