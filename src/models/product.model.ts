export class ProductModel{
  //Se modela segun base de datos.
  id:number;
  imagen:string;
  nombre:string;
  descripcion:string;
  categoria_id: string;
  estado:string;
}