import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { CategoryModel } from 'src/models/category.model';
import { CategoryService } from '../../service/category.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  categories: any;
  categoryCreate: any;
  categoryEdit: any;
  categoryActive: any;
  categoryDesactive: any;
  categoryModel: CategoryModel;
  modalOptions2: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  categoriaCreate = this.formBuilder.group({
    nombre: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required)
  });

  constructor(private categoryService: CategoryService, private modalService: NgbModal, private formBuilder: FormBuilder) { 
    this.modalOptions2 = {
      backdrop:'static',
      size: 'lg'
    }
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado()
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.categoryService.obtenerListadoCategorias().subscribe((resp: any) => {
      console.log(resp.categorias)
      this.categories = resp.data;
      this.rerender()
    })

  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  crearCategoria(form, modal) {
    if (form.nombre != "" && form.descripcion != "") {
      let data = {
        nombre: form.nombre,
        descripcion: form.descripcion
      }
      this.categoryService.crearCategoria(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss()
          this.obtenerListado()
          this.categoriaCreate.reset()
        }else{
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    }else{
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos`
      })
    }
  }

  

  eliminarCategoria(id){
    let data = {
      id : id
    }
    this.categoryService.buscarProductosPorCategoria(data).subscribe((resp: any) =>{
      if(resp.code === 200){
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }else{
        if(resp.code === 400){
          this.categoryService.eliminarCategoria(data).subscribe((resp: any) =>{
            if(resp.code === 200) {
              this.Toast.fire({
                icon: 'success',
                title: `${resp.message}`
              })
              this.obtenerListado();
            }else{
              this.Toast.fire({
                icon: 'error',
                title: `${resp.message}`
              })
            }
          })
        }
      }
    })
  }

  editarCategoria(item: CategoryModel) {
    this.categoryModel = JSON.parse(JSON.stringify(item));
  }

  guardarEdit(modal){
    this.categoryService.editarCategoria(this.categoryModel).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal){
    modal.dismiss();
    this.categoriaCreate.reset();
  }

}
