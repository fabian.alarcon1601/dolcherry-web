import { templateJitUrl } from '@angular/compiler';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { FormatoPedidoModel } from 'src/models/formatoPedido..model';
import { OrderModel } from 'src/models/order.model';
import Swal from 'sweetalert2';
import { OrderService } from '../../service/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  itemOrder: any;
  clientes: any;
  datosCliente: any;
  detalleOrden: any;
  products: any;
  orderModel: OrderModel;
  asignaEntrega: OrderModel;
  order: any;
  formatosList: [];
  formatos = new FormatoPedidoModel();
  modalOptions: any;
  modalOptions2: any;
  fechaInput: any;
  fechaValue: any;
  listRemove = [];
  faltantes:any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  orderCreate = this.formBuilder.group({
    cliente_id: new FormControl(null, Validators.required),
    fecha_entrega: new FormControl('', Validators.required),
    hora_entrega: new FormControl('', Validators.required),
    tipo_entrega: new FormControl('', Validators.required),
    productos: this.formBuilder.array([], [Validators.required, Validators.minLength(1)])
  });

  orderEdit = this.formBuilder.group({
    id: new FormControl('', Validators.required),
    cliente_id: new FormControl('', Validators.required),
    fecha_entrega: new FormControl('', Validators.required),
    hora_entrega: new FormControl('', Validators.required),
    tipo_entrega: new FormControl('', Validators.required),
    productos: this.formBuilder.array([], [Validators.required, Validators.minLength(1)])
  });

  editarEstadoPedido = this.formBuilder.group({
    recibido_por: new FormControl('', Validators.required),
  });


  constructor(private orderService: OrderService, private modalService: NgbModal, private formBuilder: FormBuilder) {
    this.modalOptions = {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    }

    this.modalOptions2 = {
      backdrop: 'static',
      size: 'lg'
    }
  }
  // --------------------------------------------------------------------------------------------

  // 
  // productos
  // 

  get productos() {
    return this.orderCreate.controls["productos"] as FormArray;
  }

  addProducto() {
    const productFormGroup = this.formBuilder.group({
      id: new FormControl(null, Validators.required),
      cantidad: new FormControl('', Validators.required),
    });
    this.productos.push(productFormGroup);
  }

  removeProducto(i: number) {
    if (this.productos.value[i].id != '' && this.productos.value[i].cantidad != '' ||
      this.productos.value[i].cantidad != '' || this.productos.value[i].id != '') {
      this.listRemove.push(this.productos.value[i]);
    }
    console.log(this.listRemove);
    this.productos.removeAt(i);
  }

  // 
  // productos
  // 

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado();
    this.obtenerListadoClientes();
    this.obtenerListadoProductos();
    this.fechaActual();
  }

  fechaActual() {
    this.fechaInput = new Date();
    this.fechaValue = new Date();
    this.fechaValue.setDate(this.fechaInput.getDate() + 2)
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.orderService.obtenerListadoOrdenes().subscribe((resp: any) => {

      this.itemOrder = resp.data;
      this.rerender()
    })
  }

  obtenerListadoClientes() {
    this.orderService.obtenerListadoClientes().subscribe((resp: any) => {
      this.clientes = resp.data;
    })
  }

  obtenerListadoProductos() {
    this.orderService.obtenerListadoProductos().subscribe((resp: any) => {
      this.products = resp.data;
    })
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  openModalFaltantes(content, faltantes) {
    this.modalService.open(content, this.modalOptions2);
  }

  openModalEntregar(content) {
    this.editarEstadoPedido.reset()
    this.modalService.open(content, this.modalOptions2);
  }

  openModalCreate(content) {
    this.modalService.open(content, this.modalOptions2);
    this.orderCreate.reset();
  }

  openModal2(content) {
    this.modalService.open(content, this.modalOptions);
  }

  openModalEdit(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  crearOrder(form, modal) {
    if (form.cliente_id != "" && form.fecha_entrega != "" && form.hora_entrega != "" && form.tipo_entrega != "") {
      let data = {
        cliente_id: form.cliente_id,
        fecha_entrega: form.fecha_entrega,
        hora_entrega: form.hora_entrega,
        tipo_entrega: form.tipo_entrega,
        usuario_id: 5,
        productos: form.productos
      }
      this.orderService.crearOrder(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss()
          this.obtenerListado()
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  asignarPedido(item: OrderModel) {
    this.asignaEntrega = item;
  }


  entregarOrder(form, modal) {
    if (form.recibido_por != "") {
      let data = {
        id: this.asignaEntrega.id,
        recibido_por: form.recibido_por
      }
      this.orderService.actualizarRecibido(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss();
          this.obtenerListado()
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  editarOrden(item: OrderModel) {
    this.orderModel = JSON.parse(JSON.stringify(item));
  }

  // seccion de editar pedido 

  setOrder(item) {

    this.order = JSON.parse(JSON.stringify(item));
    this.formatosList = item.formatos;
    this.setOrderEditForm();
  }

  setOrderEditForm() {
    this.orderEdit.reset();
    this.productos.controls.splice(0, this.productos.length);
    this.formatosList.map((item: any) => {
      const detailFormGroup = this.formBuilder.group({
        id: item.id,
        cantidad: item.formato_has_pedido.unidades,
      });
      this.productos.push(detailFormGroup);
    });
    this.orderEdit.patchValue({ 'id': this.order.id })
    this.orderEdit.patchValue({ 'cliente_id': this.order.cliente_id });
    this.orderEdit.patchValue({ 'fecha_entrega': this.order.fecha_entrega });
    this.orderEdit.patchValue({ 'hora_entrega': this.order.hora_entrega });
    this.orderEdit.patchValue({ 'tipo_entrega': this.order.tipo_entrega });
    this.orderEdit.setControl('productos', this.productos);
  }

  guardarEdit(form, modal) {
    let count = 1;
    let data = {
      id: form.id,
      cliente_id: form.cliente_id,
      fecha_entrega: form.fecha_entrega,
      hora_entrega: form.hora_entrega,
      tipo_entrega: form.tipo_entrega
    };
    this.orderService.editarPedido(data).subscribe((resp: any) => {
      if (resp.code == 200) {
        if (this.formatosList.length == this.productos.length && this.listRemove.length == 0) {
          for (let i = 0; i < this.productos.length; i++) {
            let ingrediente = {
              anterior_id: this.formatosList[i]['id'],
              formato_id: this.productos.value[i].id,
              unidades: this.productos.value[i].cantidad,
              pedido_id: form.id
            };
            this.orderService.editarFormatosDePedido(ingrediente).subscribe((resp1: any) => {
              if (resp1.code != 200) {
                this.Toast.fire({
                  icon: 'error',
                  title: resp.message
                });
                return;
              }
            })
          }
        } else if (this.formatosList.length <= this.productos.length && this.listRemove.length >= 0) {
          for (let i = 0; i < this.productos.length; i++) {
            if (this.formatosList.length >= count) {
              if (this.productos.value[i].id != this.formatosList[i]['id'] ||
                this.productos.value[i].cantidad != this.formatosList[i]['cantidad']) {
                let ingrediente = {
                  anterior_id: this.formatosList[i]['id'],
                  formato_id: this.productos.value[i].id,
                  unidades: this.productos.value[i].cantidad,
                  pedido_id: form.id
                };
                this.orderService.editarFormatosDePedido(ingrediente).subscribe((resp2: any) => {
                  if (resp2.code != 200) {
                    this.Toast.fire({
                      icon: 'error',
                      title: resp.message
                    });
                    return;
                  }
                })
              }
            } else if (this.formatosList[i] == undefined) {
              let ingrediente = {
                formato_id: this.productos.value[i].id,
                unidades: this.productos.value[i].cantidad,
                pedido_id: form.id
              };
              this.orderService.crearFormatoDePedido(ingrediente).subscribe((resp3: any) => {
                if (resp3.code != 200) {
                  this.Toast.fire({
                    icon: 'error',
                    title: resp.message
                  });
                  return;
                }
              })
            }
            count++;
          }
          for (let i = 0; i < this.listRemove.length; i++) {
            let data = {
              formato_id: this.listRemove[i].id,
              pedido_id: form.id
            };
            this.orderService.eliminarFormatoDePedido(data).subscribe((resp4: any) => {
              if (resp4.code != 200) {
                this.Toast.fire({
                  icon: 'error',
                  title: resp.message
                });
                return;
              }
            })
          }
        } else if (this.formatosList.length > this.productos.length && this.listRemove.length > 0) {
          console.log('tercer caso de prueba');
          for (let i = 0; i < this.productos.length; i++) {
            if (this.productos.length >= count) {
              if (this.productos.value[i].id != this.formatosList[i]['id'] ||
                this.productos.value[i].cantidad != this.formatosList[i]['cantidad']) {
                console.log('se ingreso al if 1');
                let dataDetail = {
                  anterior_id: this.formatosList[i]['id'],
                  formato_id: this.productos.value[i].id,
                  unidades: this.productos.value[i].cantidad,
                  pedido_id: form.id
                };
                this.orderService.editarFormatosDePedido(dataDetail).subscribe((resp5: any) => {
                  if (resp5.code != 200) {
                    this.Toast.fire({
                      icon: 'error',
                      title: resp.message
                    });
                    return;
                  }
                })
              } else if (this.formatosList[i] == undefined) {
                console.log('se ingreso al segundo if');
                let dataDetail = {
                  formato_id: this.productos.value[i].id,
                  unidades: this.productos.value[i].cantidad,
                  pedido_id: form.id
                };
                this.orderService.crearFormatoDePedido(dataDetail).subscribe((resp6: any) => {
                  if (resp6.code != 200) {
                    this.Toast.fire({
                      icon: 'error',
                      title: resp.message
                    });
                    return;
                  }
                })
              }
            }
            count++;
          }
          for (let i = 0; i < this.listRemove.length; i++) {
            console.log('se comienza a eliminar');
            let data = {
              formato_id: this.listRemove[i].id,
              pedido_id: form.id
            };
            this.orderService.eliminarFormatoDePedido(data).subscribe((resp7: any) => {
              if (resp7.code != 200) {
                this.Toast.fire({
                  icon: 'error',
                  title: resp.message
                });
                return;
              }
            })
          }
        }
        console.log(this.productos.length == count + this.listRemove.length);
        console.log('this.listRemove.length: ', this.listRemove.length);
        console.log('this.productos.length: ', this.productos.length);
        console.log('count: ', count);
        if (this.productos.length == count + this.listRemove.length) {
          modal.dismiss()
          this.obtenerListado();
          this.productos.controls.splice(0, this.productos.length);
          this.listRemove = [];
          this.orderEdit.reset();
          this.Toast.fire({
            icon: 'success',
            title: resp.message
          });
        }
        this.obtenerListado()
        modal.dismiss()
        this.Toast.fire({
          icon: 'success',
          title: resp.message
        });
      } else {
        this.Toast.fire({
          icon: 'error',
          title: 'error al modificar los datos'
        });
      }
    });
  }

  // seccion editar pedido fin    

  dismiss(modal) {
    modal.dismiss();
    this.orderCreate.reset();
    this.orderEdit.reset();
  }

  verPedido(item) {
    this.datosCliente = item;
    console.log(this.datosCliente);
    let data = {
      id: item.id
    }
    this.orderService.obtenerFormatosPorPedido(data).subscribe((resp: any) => {
      this.detalleOrden = resp.data.formatos;
    })
  }

  cancelarEntrega(item) {
    Swal.fire({
      title: 'Estas seguro de cancelar la entrega?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar'
    }).then((result) => {
      if (result.isConfirmed) {
        let data = {
          id: item.id
        }
        this.orderService.cancelarEntregaPedido(data).subscribe((resp: any) => {
          this.obtenerListado()
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
        })
      }
    })
  }

  eliminarOrden(id) {
    console.log(id);
    let data = {
      id: id
    }
    Swal.fire({
      title: 'Estas seguro de querer eliminar el ingrediente?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.orderService.eliminarFormatosDeUnPedido(data).subscribe((resp: any) => {
          if (resp.code === 200) {
            this.orderService.eliminarPedido(data).subscribe((resp2: any) => {
              this.Toast.fire({
                icon: 'success',
                title: `${resp2.message}`
              })
              this.obtenerListado()
            })
          } else if (resp.code === 0) {
            this.orderService.eliminarPedido(data).subscribe((resp2: any) => {
              this.Toast.fire({
                icon: 'success',
                title: `${resp2.message}`
              })
              this.obtenerListado()
            })
          }else{
            this.Toast.fire({
              icon: 'success',
              title: `Problemas inesperados al intentar eliminar`
            })
          }
        })
      }
    })
  }

  finalizarPedido(id){
    let data = {
      id:id
    }
    this.orderService.finalizarPedido(data).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.code ===200 ){
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  pasarPreparacion(id, contenido){
    let data ={
      id:id
    }
    this.orderService.pasarPreparacion(data).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.code ===200 ){
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado()
      }else{
        this.faltantes = resp
        this.openModalFaltantes(contenido, this.faltantes)
        
      }
    })
  }
}
