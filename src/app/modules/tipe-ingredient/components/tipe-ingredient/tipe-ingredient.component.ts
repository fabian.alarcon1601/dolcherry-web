import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { IngredientModel } from 'src/models/ingredient.model';
import { TipeIngredientService } from '../../service/tipe-ingredient.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { IngredientValidator } from '../../validations';

@Component({
  selector: 'app-tipe-ingredient',
  templateUrl: './tipe-ingredient.component.html',
  styleUrls: ['./tipe-ingredient.component.scss']
})
export class TipeIngredientComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  tipeIngredients: any;
  marcas: any;
  ingredientModel: IngredientModel;
  valorizacion:any;
  modalOptions2:any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  tipeIngredientCreate = this.formBuilder.group({
    nombre: new FormControl('', Validators.required),
    marca_id: new FormControl('null', [Validators.required , IngredientValidator.verificarSelect]),
    unidad_medida: new FormControl('null', [Validators.required , IngredientValidator.verificarSelect]),
    stock_minimo: new FormControl('', [Validators.required , Validators.pattern('[- +()0-9]+'), Validators.maxLength(3)])
  });

  constructor(private tipeIngredientService: TipeIngredientService, private modalService: NgbModal, private formBuilder: FormBuilder, private router: Router) {
    this.modalOptions2 = {
      backdrop:'static',
      size: 'lg'
    }
  }
  
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado()
    this.obtenerListadoMarcas()
  }

  navigate(item){
    console.log(item);
    let ingredient = item.nombre.replaceAll(' ', '-').toLowerCase()+'-'+item.marca.nombre.replaceAll(' ', '-').toLowerCase() ;
    this.router.navigate(['/tipe-ingredient/stock', ingredient],{
      state:{
        item
      }
    });
  }

  valorizarInventario(){
    this.tipeIngredientService.valorizarIngredientes().subscribe((resp: any) => {
      if(resp.code === 200){
          this.tipeIngredientService.valorizarFormatosProductos().subscribe((resp:any)=>{
            Swal.close()
            this.obtenerListado()
          })
      }
    })
    let timerInterval
    Swal.fire({
      title: 'Valorizando inventario',
      html: 'Espere un momento por favor ',
      backdrop: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading()
        const b = Swal.getHtmlContainer().querySelector('b')
      },
      willClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      
    })
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.tipeIngredientService.obtenerListadoTiposIngredientes().subscribe((resp: any) => {
      this.tipeIngredients = resp.data;
      this.rerender()
    })
  }

  obtenerListadoMarcas() {
    this.tipeIngredientService.obtenerListadoMarcas().subscribe((resp: any) => {
      this.marcas = resp.data;
    })
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  openModalEdit(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  crearTipeIngredient(form, modal) {
    if (form.nombre != "" && form.marca_id != "" && form.stock_minimo != "") {
      let data = {
        nombre: form.nombre,
        marca_id: form.marca_id,
        unidad_medida:form.unidad_medida,
        stock_minimo:form.stock_minimo
      }
      this.tipeIngredientService.crearTipoIngrediente(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss()
          this.obtenerListado()
          this.tipeIngredientCreate.reset()
        }else{
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    }else{
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  editarTipoIngrediente(item: IngredientModel) {
    this.ingredientModel = JSON.parse(JSON.stringify(item));
  }

  eliminarTipoIngrediente(id){
    let data = {
      id : id
    }
    console.log(data);
    this.tipeIngredientService.buscarFormatosPorIngrediente(data).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.code === 200){
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }else{
        if(resp.code === 404){
          // comprobar que no forme parte de un formato de producto 
          this.tipeIngredientService.buscarIngredienteStock(data).subscribe((resp1: any) =>{
            console.log(resp1);
            if(resp1.code === 200){
              this.Toast.fire({
                icon: 'error',
                title: `${resp1.message}`
              })
            }else if(resp1.code === 400){
              Swal.fire({
                title: 'Estas seguro de querer eliminar el ingrediente?',
                text: "¡No podrás revertir esto!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar'
              }).then((result) => {
                if (result.isConfirmed) {
                  this.tipeIngredientService.eliminarTipoIngrediente(data).subscribe((resp2: any) =>{
                    console.log('resp2: ', resp2);
                    if(resp2.code === 200) {
                      this.Toast.fire({
                        icon: 'success',
                        title: `${resp2.message}`
                      })
                      this.obtenerListado();
                    }else{
                      this.Toast.fire({
                        icon: 'error',
                        title: `${resp2.message}`
                      })
                    }
                  })
                }
              })
            }
          })
        }
      }
    })
  }

  guardarEdit(modal) {
    this.tipeIngredientService.editarTipoIngrediente(this.ingredientModel).subscribe((resp: any) => {
      console.log(resp)
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal){
    modal.dismiss();
    this.tipeIngredientCreate.reset();
  }
}
