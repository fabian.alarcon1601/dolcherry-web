export class OrderModel{
  //Se modela segun base de datos.
  id:number;
  fecha_entrega:Date;
  hora_entrega: string;
  tipo_entrega: string;
  estado:boolean;
  usuario_id:number;
  cliente_id:number;
}