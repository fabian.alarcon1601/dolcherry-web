import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoUsuarios() {
    return this.http.get(url + '/usuario/listar')
  }
  
  obtenerListadoFooter() {
    return this.http.get(url + '/footer/listar')
  }

  obtenerListadoRoles() {
    return this.http.get(url + '/rol/listar')
  }

  crearUsuario(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/usuario/crear', data, option);
  }

  desactivarUsuario(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/usuario/desactivar', data, option);
  }

  activarUsuario(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/usuario/activar', data, option);
  }

  obtenerListadoInicio() {
    return this.http.get(url + '/inicio/listar')
  }

  editarFooter(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/footer/editar', data, option);
  }

  editarAbout(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/nosotros/editar', data, option);
  }

  obtenerListadoAbout() {
    return this.http.get(url + '/nosotros/listar')
  }

  editarHome(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/inicio/editar', data, option);
  }

  editarUser(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/usuario/editar', data, option);
  }

  crearHome(data){
    let strContent = 'application/json';
    let header = new HttpHeaders().set('Accept', strContent)
    const option = {headers:header};
    return this.http.post(url+'/inicio/crear', data, option);
  }

  crearFooter(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/footer/crear', data, option);
  }

  crearAbout(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/nosotros/crear', data, option);
  }
}
