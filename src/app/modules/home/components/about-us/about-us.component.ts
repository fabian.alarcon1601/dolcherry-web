import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HomeService } from '../../service/home.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  google:any;
  homeList: any;
  imagen: any;

  constructor(private homeService: HomeService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.listHome()
  }

  listHome(){
    this.homeService.obtenerListadoInicio().subscribe((resp: any) => {
      if(resp.code === 200){
        this.homeList = resp.data;
        this.imagen = this.obtenerImagen(resp.data.imagen)
      }else{ 
        this.imagen = 'http://localhost:3001/Images/home/1659213454340.jpeg'
      }
    })
  }

  obtenerImagen(item){
    let resultado = item.replaceAll( '\\', "/")
    let ruta = 'http://localhost:3001/'
    return ruta+resultado;
  }

}
