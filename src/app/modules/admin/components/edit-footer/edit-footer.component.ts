import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-edit-footer',
  templateUrl: './edit-footer.component.html',
  styleUrls: ['./edit-footer.component.scss']
})
export class EditFooterComponent implements OnInit {
  isEdit = false;
  datosEdit:any;
  mensaje:any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  editFooterForm = this.formBuilder.group({
    isEdit:this.isEdit,
    instagram: new FormControl({value: null,disabled: !this.isEdit},[Validators.required]),
    facebook: new FormControl({value: null,disabled: !this.isEdit},[Validators.required]),
    whatsapp: new FormControl({value: null,disabled: !this.isEdit},[Validators.required]),
    ubicacion_google: new FormControl({value: null,disabled: !this.isEdit},[Validators.required]),
    numero_celular: new FormControl({value: null,disabled: !this.isEdit},[Validators.required, Validators.pattern('[- +()0-9]+') ,Validators.minLength(8), Validators.maxLength(8)]),
    correo: new FormControl({value: null,disabled: !this.isEdit},[Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
  });

  constructor(private adminService: AdminService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.obtenerListado();
  }

  editFooter(value){
    this.isEdit = true;
    const instagram = this.editFooterForm.get('instagram');
    const facebook = this.editFooterForm.get('facebook');
    const whatsapp = this.editFooterForm.get('whatsapp');
    const ubicacion_google = this.editFooterForm.get('ubicacion_google');
    const numero_celular = this.editFooterForm.get('numero_celular');
    const correo = this.editFooterForm.get('correo');

    instagram.enable();
    facebook.enable();
    whatsapp.enable();
    ubicacion_google.enable();
    numero_celular.enable();
    correo.enable();
  }

  saveFooter(values){
    console.log(values);
    let data ={
     instagram:values.instagram,
     facebook:values.facebook,
     whatsapp:values.whatsapp,
     ubicacion_google:values.ubicacion_google,
     numero_celular:values.numero_celular,
     correo:values.correo
    }
    console.log(data);
    this.adminService.editarFooter(data).subscribe((resp:any)=>{
      this.mensaje = resp.message

      if (resp.code===200){
        this.isEdit = false;
        const instagram = this.editFooterForm.get('instagram');
        const facebook = this.editFooterForm.get('facebook');
        const whatsapp = this.editFooterForm.get('whatsapp');
        const ubicacion_google = this.editFooterForm.get('ubicacion_google');
        const numero_celular = this.editFooterForm.get('numero_celular');
        const correo = this.editFooterForm.get('correo');
        instagram.disable();
        facebook.disable();
        whatsapp.disable();
        ubicacion_google.disable();
        numero_celular.disable();
        correo.disable();
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado()
      }else {
        if(resp.code!=200){
          (this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          }))
        }  
      }
    })
    this.Toast.fire({
      icon: 'error',
      title: `${this.mensaje}`
    })
  }

  crearFooter(value){
    let data = {
      instagram: value.instagram,
      whatsapp: value.whatsapp,
      facebook: value.facebook,
      ubicacion_google: value.ubicacion_google,
      numero_celular: value.numero_celular,
      correo: value.correo,
    }
    this.adminService.crearFooter(data).subscribe((resp:any)=>{
      if(resp.code === 200){
        this.isEdit = false;
        const instagram = this.editFooterForm.get('instagram');
        const facebook = this.editFooterForm.get('facebook');
        const whatsapp = this.editFooterForm.get('whatsapp');
        const ubicacion_google = this.editFooterForm.get('ubicacion_google');
        const numero_celular = this.editFooterForm.get('numero_celular');
        const correo = this.editFooterForm.get('correo');
        instagram.disable();
        facebook.disable();
        whatsapp.disable();
        ubicacion_google.disable();
        numero_celular.disable();
        correo.disable();
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado()
      }else {
        if(resp.code!=200){
          (this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          }))
        }  
      }
    })
  }

  obtenerListado(){
    this.adminService.obtenerListadoFooter().subscribe((resp:any)=>{
      if(resp.code===200){
        this.datosEdit = resp.data;
        this.editFooterForm.patchValue(this.datosEdit)
      }
    })
  }

  cancel(){
    this.isEdit = false
    this.editFooterForm.reset()

    const instagram = this.editFooterForm.get('instagram');
    const facebook = this.editFooterForm.get('facebook');
    const whatsapp = this.editFooterForm.get('whatsapp');
    const ubicacion_google = this.editFooterForm.get('ubicacion_google');
    const numero_celular = this.editFooterForm.get('numero_celular');
    const correo = this.editFooterForm.get('correo');

    instagram.disable();
    facebook.disable();
    whatsapp.disable();
    ubicacion_google.disable();
    numero_celular.disable();
    correo.disable();
    this.obtenerListado()
  }

}
