import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../../service/admin.service';
import { HomeModel } from 'src/models/home.model'
import Swal from 'sweetalert2';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-edit-home',
  templateUrl: './edit-home.component.html',
  styleUrls: ['./edit-home.component.scss']
})



export class EditHomeComponent implements OnInit {
  public archivos:any = [];
  public previsualizacion:string;

  imagen:any;
  editHome : any;
  homeModel: HomeModel;
  modalOptions2: any;
  imagenHome : any;
  imagenCasoCancelar:any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  homeCreate = this.formBuilder.group({
    imagen: new FormControl('', Validators.required),
    titulo: new FormControl('', Validators.required),
    subtitulo: new FormControl('', Validators.required)
  });

  @ViewChild('imagen') fileinput:ElementRef;
  constructor(private sanitizer:DomSanitizer,private adminService : AdminService,  private modalService: NgbModal, private formBuilder: FormBuilder) { 
    this.modalOptions2 = {
      backdrop:'static',
      size: 'lg'
    }
   }

  ngOnInit(): void {
    this.obtenerDatosHome();
  }

  obtenerDatosHome(){
    this.adminService.obtenerListadoInicio().subscribe((resp: any) =>{
      if(resp.code === 200){
        this.editHome = resp.data;
        this.homeModel = resp.data
        this.imagen = this.obtenerImagen( resp.data.imagen)
        this.imagenCasoCancelar =   resp.data.imagen
      }else{
        this.imagen = 'http://localhost:3001/Images/home/1659213454340.jpeg'
      }
    })  
  }

  // captura de imagen 
  // ---------------------------------------------------------------------------------------------

  capturarFile(event) {
    const archivoCapturado = event.target.files[0]
    this.extraerBase64(archivoCapturado).then((imagen: any) => {
      this.previsualizacion = imagen.base;
    })
    this.archivos.push(archivoCapturado);
  }

  fileChange(file: any) {
    this.imagenHome = file[0]
  }

  fileChangeCreate(file:any){
    this.imagenHome = file[0]
  }

  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          base: null
        });
      };
    } catch (e) {
      return null;
    }
  })


  obtenerImagen(item){
    let resultado = item.replaceAll( '\\', "/")
    let ruta = 'http://localhost:3001/'
    return ruta + resultado;
  }

  // captura de imagen
  // --------------------------------------------------------------------------------------------

  editarHome(item: HomeModel) {
    this.homeModel = item;
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  crearHome(form, modal){
    if (form.titulo != "" &&  form.subtitulo != "" && form.imagen != "") {
      let formdata = new FormData();
        formdata.append('titulo', form.titulo)
        formdata.append('imagen', this.imagenHome)
        formdata.append('subtitulo', form.subtitulo)
      this.adminService.crearHome(formdata).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss()
          this.obtenerDatosHome()
          this.homeCreate.reset()
        }else{
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    }else{
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos`
      })
    }
  }

  guardarEdit(modal,titulo, subtitulo, imagen){
    this.adminService.editarHome(this.homeModel).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerDatosHome()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal){
    modal.dismiss();
    this.homeModel.imagen = this.imagenCasoCancelar;
    this.previsualizacion = null;
    this.homeModel = this.editHome
  }
}
