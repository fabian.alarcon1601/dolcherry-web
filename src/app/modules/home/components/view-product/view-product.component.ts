import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from '../../service/home.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss']
})
export class ViewProductComponent implements OnInit {
  param:any;
  imagen:any;
  formatos:any;

  constructor(private router: Router, private homeService: HomeService) { 
    this.receiverParameters();
  }

  receiverParameters() {
    let para = this.router.getCurrentNavigation().extras.state;
    if (para != undefined) {
      this.param = para.item
      console.log('this.param: ', this.param);
      this.imagen = this.obtenerImagen(this.param.imagen)
    } else {
      this.router.navigateByUrl('/')
    }
  }

  obtenerImagen(item) {
    let resultado = item.replace('\\', "/")
    let ruta = 'http://localhost:3001/'
    return ruta + resultado;
  }

  ngOnInit(): void {
    this.obtenerListado(this.param.id);
  }


  obtenerListado(item) {
    let data = {
      id: item
    }
    this.homeService.obtenerFormatosPorProducto(data).subscribe((resp: any) => {
      this.formatos = resp.data;
    })
  }
}
