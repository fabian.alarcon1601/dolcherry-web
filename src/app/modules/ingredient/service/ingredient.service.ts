import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoIngredientes() {
    return this.http.get(url + '/ingrediente/listar')
  }

  crearIngrediente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/ingrediente/crear', data, option);
  }

  editarIngrediente(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/ingrediente/editar', data, option);
  }

  eliminarIngrediente(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/ingrediente/eliminar', data, option);
  }

  obtenerListadoTipoIngrediente(){
    return this.http.get(url + '/tipoIngrediente/listar')
  }

  obtenerListadoMarcas(){
    return this.http.get(url + '/marca/listar')
  }

  actualizarIngrediente(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/tipoIngrediente/actualizar', data, option);
  }


}
