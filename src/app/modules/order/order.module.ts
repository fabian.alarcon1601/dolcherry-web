import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { OrderComponent } from './components/order/order.component';
import { TodayComponent } from './components/today/today.component';
import { TomorrowComponent } from './components/tomorrow/tomorrow.component';


@NgModule({
  declarations: [OrderComponent, TodayComponent, TomorrowComponent],
  imports: [
    CommonModule,
    OrderRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class OrderModule { }
