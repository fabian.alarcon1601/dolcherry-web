import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { BrandService } from '../../services/brand.service';
import { BrandModel } from 'src/models/brand.model'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  brands: any;
  brandModel: BrandModel;
  modalOptions2: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })


  brandCreate = this.formBuilder.group({
    nombre: new FormControl('', Validators.required),
  });

  constructor(private brandService: BrandService, private modalService: NgbModal, private formBuilder: FormBuilder) {
    this.modalOptions2 = {
      backdrop:'static',
      size: 'lg'
    }
   }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado()
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.brandService.obtenerListadoMarcas().subscribe((resp: any) => {
      console.log(resp)
      this.brands = resp.data;
      this.rerender()
    })
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  openModalEdit(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  crearMarca(form, modal) {
    if (form.nombre != "") {
      let data = {
        nombre: form.nombre,
      }
      this.brandService.crearMarca(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss();
          this.obtenerListado();
          this.brandCreate.reset();
        }else{
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    }else{
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los campos solicitados`
      })
    }

  }

  eliminarMarca(id){
    let data = {
      id : id
    }
    this.brandService.buscarIngredienteByMarca(data).subscribe((resp: any) =>{
      if(resp.code === 200){
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }else{
        if(resp.code === 400){
          this.brandService.eliminarMarca(data).subscribe((resp: any) =>{
            if(resp.code === 200) {
              this.Toast.fire({
                icon: 'success',
                title: `${resp.message}`
              })
              this.obtenerListado();
            }else{
              this.Toast.fire({
                icon: 'error',
                title: `${resp.message}`
              })
            }
          })
        }
      }
    })
  }

  editarMarca(item: BrandModel) {
    this.brandModel = JSON.parse(JSON.stringify(item));
  }

  guardarEdit(modal) {
    this.brandService.editarMarca(this.brandModel).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal){
    modal.dismiss();
    this.brandCreate.reset();
  }
}
