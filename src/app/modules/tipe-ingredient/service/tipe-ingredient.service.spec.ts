import { TestBed } from '@angular/core/testing';

import { TipeIngredientService } from './tipe-ingredient.service';

describe('TipeIngredientService', () => {
  let service: TipeIngredientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipeIngredientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
