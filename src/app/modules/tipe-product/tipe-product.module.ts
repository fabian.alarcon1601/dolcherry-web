import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipeProductRoutingModule } from './tipe-product-routing.module';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TipeProductComponent } from './components/tipe-product/tipe-product.component';
import { FormatoComponent } from './components/formato/formato.component';


@NgModule({
  declarations: [TipeProductComponent, FormatoComponent],
  imports: [
    CommonModule,
    TipeProductRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule
  ]
})
export class TipeProductModule { }
