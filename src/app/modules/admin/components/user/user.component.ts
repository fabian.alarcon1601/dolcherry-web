import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { AdminService } from '../../service/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  users: any;
  roles: any;
  id_user: any;
  modalOptions2: any;
  loading : boolean = false;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  userCreate = this.formBuilder.group({
    nombre: new FormControl('', Validators.required),
    apellido_paterno: new FormControl('', Validators.required),
    apellido_materno: new FormControl('', Validators.required),
    telefono: new FormControl('', [Validators.required, Validators.pattern('[- +()0-9]+'), Validators.minLength(8), Validators.maxLength(8)]),
    correo: new FormControl('', [Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    rol_id: new FormControl('', Validators.required),
  });

  userEdit = this.formBuilder.group({
    id :  new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
    apellido_paterno: new FormControl('', Validators.required),
    apellido_materno: new FormControl('', Validators.required),
    telefono: new FormControl('', [Validators.required, Validators.pattern('[- +()0-9]+'), Validators.minLength(8), Validators.maxLength(8)]),
    correo: new FormControl('', [Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    rol_id: new FormControl('', Validators.required),
  });

  constructor(private adminService: AdminService, private modalService: NgbModal, private formBuilder: FormBuilder) { 
    this.modalOptions2 = {
      backdrop:'static',
      size: 'lg'
    }
   }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado();
    this.obtenerListadoRoles();
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.adminService.obtenerListadoUsuarios().subscribe((resp: any) => {
      this.users = resp.data;
      this.rerender();
    })
  }

  obtenerListadoRoles() {
    this.adminService.obtenerListadoRoles().subscribe((resp: any) => {
      this.roles = resp.data;
      this.rerender();
    })
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  openModalCreate(content) {
    this.modalService.open(content, this.modalOptions2);
    this.userCreate.reset();
  }

  crearUsuario(form, modal) {
    this.loading = true;
    if (form.correo != "" && form.rol_id != "" && form.nombre != "" && form.apellido_paterno != "" && form.apellido_materno != "" && form.telefono != "") {
      let data = {
        correo: form.correo,
        rol_id: form.rol_id,
        nombre: form.nombre,
        apellido_paterno: form.apellido_paterno,
        apellido_materno: form.apellido_materno,
        telefono: form.telefono,
      }
      this.adminService.crearUsuario(data).subscribe(async (resp: any) => {
        console.log(resp);
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          this.loading = false;
          modal.dismiss()
          this.obtenerListado()
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  desactivar(id) {
    let data = {
      id: id
    }
    this.adminService.desactivarUsuario(data).subscribe(async (resp: any) => {
      console.log(resp);
      if (resp.code === 200) {
        this.obtenerListado()
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
      }
    });
  }

  activar(id) {
    let data = {
      id: id
    }
    this.adminService.activarUsuario(data).subscribe(async (resp: any) => {
      console.log(resp);
      if (resp.code === 200) {
        this.obtenerListado()
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
      }
    });
  }

  editarUser(value) {
    console.log(value);
    this.userEdit.patchValue(value);
    console.log(this.userEdit);
  }

  guardarEdit(form, modal){
    console.log(form);
    let data = {
      id: form.id,
      apellido_paterno: form.apellido_paterno,
      apellido_materno: form.apellido_materno,
      correo: form.correo,
      telefono: form.telefono,
      nombre: form.nombre,
      rol_id: form.rol_id
    }
    this.adminService.editarUser(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal){
    modal.dismiss();
    this.userCreate.reset();
  }

  getError(controlName: string): string {
    let error = '';
    const control = this.userEdit.get(controlName);
    if (control.touched && control.errors != null) {
      error = JSON.stringify(control.errors);
    }
    return error;
  }
}
