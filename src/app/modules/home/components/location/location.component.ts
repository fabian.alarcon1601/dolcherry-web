import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../service/home.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  constructor(private homeService: HomeService) { }
  maps: any;
  footer: any;
  ngOnInit(): void {
    this.obtenerLocation();
  }

  obtenerLocation() {
    this.homeService.obtenerListadoFooter().subscribe((resp: any) => {
      
      if (resp.code === 2 && resp.data.ubicacion_google.length > 0) {
        this.maps = resp.data.ubicacion_google
      } else {
        this.maps = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3202.895272285751!2d-72.07547308411176!3d-36.604831273607395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x966929df9cd4b321%3A0x5bf37d23e1be2cb0!2zTGFzIFRlcm1hcyAyMzQsIENoaWxsYW4sIENoaWxsw6FuLCBCw61vIELDrW8!5e0!3m2!1ses-419!2scl!4v1662230284355!5m2!1ses-419!2scl" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>'
      }
    })
  }
}


