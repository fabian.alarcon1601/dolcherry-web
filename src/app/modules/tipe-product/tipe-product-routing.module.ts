import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipeProductComponent } from './components/tipe-product/tipe-product.component';
import { FormatoComponent } from './components/formato/formato.component';

const routes: Routes = [
  {path: '', component: TipeProductComponent},
  {path: 'formato/:format', component: FormatoComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TipeProductRoutingModule { }
