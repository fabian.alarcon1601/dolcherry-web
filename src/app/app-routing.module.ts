import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent ,canActivate:[AuthGuard], data:{role:['administrador', 'profesional']}},
  { path: 'basic-ui', loadChildren: () => import('./basic-ui/basic-ui.module').then(m => m.BasicUiModule) },
  { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartsDemoModule) },
  { path: 'forms', loadChildren: () => import('./forms/form.module').then(m => m.FormModule) },
  { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule) },
  { path: 'icons', loadChildren: () => import('./icons/icons.module').then(m => m.IconsModule) },
  { path: 'general-pages', loadChildren: () => import('./general-pages/general-pages.module').then(m => m.GeneralPagesModule) },
  { path: 'apps', loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule) },
  { path: 'user-pages', loadChildren: () => import('./user-pages/user-pages.module').then(m => m.UserPagesModule) },
  { path: 'error-pages', loadChildren: () => import('./error-pages/error-pages.module').then(m => m.ErrorPagesModule) },
  { path: 'role', loadChildren: () => import('./modules/role/role-routing.module').then(m => m.RoleRoutingModule), canActivate:[AuthGuard], data:{role:['administrador']}},
  { path: 'ingredient', loadChildren: () => import('./modules/ingredient/ingredient-routing.module').then(m => m.IngredientRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} },
  { path: 'category', loadChildren: () => import('./modules/category/category-routing.module').then(m => m.CategoryRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} },
  { path: 'product', loadChildren: () => import('./modules/product/product-routing.module').then(m => m.ProductRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} },
  { path: 'admin', loadChildren: () => import('./modules/admin/admin-routing.module').then(m => m.AdminRoutingModule), canActivate:[AuthGuard], data:{role:['administrador']} },
  { path: '', loadChildren: () => import('./modules/home/home-routing.module').then(m => m.HomeRoutingModule) },
  { path: 'tipe-ingredient', loadChildren: () => import('./modules/tipe-ingredient/tipe-ingredient-routing.module').then(m => m.TipeIngredientRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} },
  { path: 'brand', loadChildren: () => import('./modules/brand/brand-routing.module').then(m => m.BrandRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} },
  { path: 'client', loadChildren: () => import('./modules/client/client-routing.module').then(m => m.ClientRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} },
  { path: 'order', loadChildren: () => import('./modules/order/order-routing.module').then(m => m.OrderRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']}},
  { path: 'profile', loadChildren: () => import('./modules/profile/profile-routing.module').then(m => m.ProfileRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} },
  { path: 'tipe-product', loadChildren: () => import('./modules/tipe-product/tipe-product-routing.module').then(m => m.TipeProductRoutingModule), canActivate:[AuthGuard], data:{role:['administrador', 'profesional']} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
