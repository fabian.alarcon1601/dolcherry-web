import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../service/home.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
footer:any;
  constructor(private homeService: HomeService) { }

  ngOnInit(): void {
    this.obtenerFooter();
  }

  obtenerFooter(){
    this.homeService.obtenerListadoFooter().subscribe((resp:any)=>{
      if (resp.code===200){
        this.footer = resp.data;
      }
    })
  }
}
