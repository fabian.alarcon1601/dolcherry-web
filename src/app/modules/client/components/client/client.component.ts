import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { ClientModel } from 'src/models/client.model';
import { ClientService } from '../../service/client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  clients: any;
  clientModel: ClientModel;
  modalOptions2: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  clientCreate = this.formBuilder.group({
    nombre_completo: new FormControl('', Validators.required),
    correo: new FormControl('', [Validators.required ,Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    direccion: new FormControl('', Validators.required),
    telefono: new FormControl('', [Validators.required , Validators.pattern('[- +()0-9]+') ,Validators.minLength(8), Validators.maxLength(8)])
  });

  constructor(private clientService: ClientService, private modalService: NgbModal, private formBuilder: FormBuilder) { 
    this.modalOptions2 = {
      backdrop:'static',
      size: 'lg'
    }
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado()
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.clientService.obtenerListadoClientes().subscribe((resp: any) => {
      this.clients = resp.data;
      this.rerender()
    })

  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  openModalEdit(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  crearCliente(form, modal) {
    if (form.nombre != "" && form.correo != "" && form.direccion != "" && form.telefono != "") {
      let data = {
        nombre_completo: form.nombre_completo,
        correo: form.correo,
        direccion: form.direccion,
        telefono: form.telefono
      }
      this.clientService.crearCliente(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss()
          this.clientCreate.reset()
          this.obtenerListado()
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    }else{
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  editarCliente(item: ClientModel) {
    this.clientModel = item;
  }

  guardarEdit(modal) {
    this.clientService.editarCliente(this.clientModel).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  eliminarCliente(item){
    let data = {
      id : item.id
    }
    this.clientService.pedidosCliente(data).subscribe((resp:any)=>{
      if(resp.code===400){
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.clientService.eliminarCliente(data).subscribe((resp2:any)=>{
          this.obtenerListado()
        })
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }
}
