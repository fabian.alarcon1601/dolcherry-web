import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { FormatModel } from 'src/models/format.model';
import Swal from 'sweetalert2';
import { TipeProductService } from '../../services/tipe-product.service';

@Component({
  selector: 'app-formato',
  templateUrl: './formato.component.html',
  styleUrls: ['./formato.component.scss']
})
export class FormatoComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  productModel: FormatModel;
  itemProducts: any;
  infoFormato: any;
  param: any;
  formatos: any;
  categorias: any;
  ingredientesFormato: any;
  tipoProducto: any;
  tipoIngrediente: any;
  ingredientArray = []
  formato: any;
  formatosList: []
  modalOptions2: any;
  listRemove = [];

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  productCreate = this.formBuilder.group({
    nombre: new FormControl('', Validators.required),
    valor: new FormControl('', Validators.required),
    ingredientes: this.formBuilder.array([], [Validators.required, Validators.minLength(1)])
  });

  productEdit = this.formBuilder.group({
    id: new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
    valor: new FormControl('', Validators.required),
    descuento: new FormControl('', Validators.required),
    ingredientes: this.formBuilder.array([], [Validators.required, Validators.minLength(1)])
  });


  constructor(private tipeProductService: TipeProductService, private modalService: NgbModal, private formBuilder: FormBuilder, private router: Router) {
    this.receiverParameters();
    this.modalOptions2 = {
      backdrop: 'static',
      size: 'lg'
    }
  }

  receiverParameters() {
    let para = this.router.getCurrentNavigation().extras.state;
    if (para != undefined) {
      this.param = para.item
    } else {
      this.router.navigateByUrl('/tipe-product')
    }
  }

  // --------------------------------------------------------------------------------------------

  // 
  // ingredientes
  // 

  get ingredientes() {
    return this.productCreate.controls["ingredientes"] as FormArray;
  }

  addIngrediente() {
    const ingredientFormGroup = this.formBuilder.group({
      id: new FormControl(null,Validators.required),
      cantidad: new FormControl('', Validators.required),
    });
    console.log(ingredientFormGroup);
    this.ingredientes.push(ingredientFormGroup);
  }

  removeIngrediente(i: number) {
    this.ingredientes.removeAt(i);
  }

  removeIngredienteEdit(i: number) {
    if (this.ingredientes.value[i].id != '' && this.ingredientes.value[i].cantidad != '' ||
      this.ingredientes.value[i].cantidad != '' || this.ingredientes.value[i].id != '') {
      this.listRemove.push(this.ingredientes.value[i]);
    }
    this.ingredientes.removeAt(i);
  }

  // 
  // ingredientes
  // 

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado(this.param.id);
    this.obtenerListadoTipoIngrediente();
    this.obtenerListadoTipoProducto();
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado(item) {
    let data = {
      id: item
    }
    this.tipeProductService.obtenerFormatosPorProducto(data).subscribe((resp: any) => {
      this.formatos = resp.data;
      this.rerender()
    })
  }

  obtenerListadoTipoProducto() {
    this.tipeProductService.obtenerListadoTipoProducto().subscribe((resp: any) => {
      console.log(resp.data);
      this.tipoProducto = resp.data;
    })
  }

  obtenerListadoTipoIngrediente() {
    this.tipeProductService.obtenerListadoTiposIngredientes().subscribe((resp: any) => {
      console.log(resp.data);
      this.tipoIngrediente = resp.data;
    })
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0)
    }
  }

  openModalCrear(content) {
    this.productCreate.reset();
    this.clearFormArray(this.ingredientes);
    this.modalService.open(content, this.modalOptions2);
  }

  crearProducto(form, modal) {
    console.log(this.ingredientes);
    if (form.nombre != "" && form.valor != "" && form.categoria_id != "") {
      let data = {
        nombre: form.nombre,
        valor: form.valor,
        producto_id: this.param.id,
        ingredientes: form.ingredientes
      }
      this.tipeProductService.crearFormato(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss()
          this.obtenerListado(this.param.id)
          this.productCreate.reset()
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos`
      })
    }
  }

  verIngredientes(item) {
    this.infoFormato = item;
    let data = {
      id: item.id
    }
    this.tipeProductService.obtenerIngredientesPorFormato(data).subscribe((resp: any) => {
      this.ingredientesFormato = resp.data[0].ingredientes;
    })
  }

  editarProducto(item: FormatModel) {
    this.productModel = JSON.parse(JSON.stringify(item));
  }

  setFormato(item) {
    console.log(item);
    // para no utilizar el mismo espacio de memoria
    this.formato = JSON.parse(JSON.stringify(item));
    this.formatosList = item.ingredientes;
    this.setOrderCreateForm();
  }

  setOrderCreateForm() {
    this.productEdit.reset();
    this.ingredientes.controls.splice(0, this.ingredientes.length);
    this.formatosList.map((item: any) => {
      const ingredientesFormGroup = this.formBuilder.group({
        id: item.id,
        cantidad: item.formato_has_ingrediente.cantidad,
      });
      this.ingredientes.push(ingredientesFormGroup);
    });

    this.productEdit.patchValue({ 'id': this.formato.id });
    this.productEdit.patchValue({ 'nombre': this.formato.nombre });
    this.productEdit.patchValue({ 'valor': this.formato.valor });
    this.productEdit.patchValue({ 'descuento': this.formato.descuento });
    this.productEdit.setControl('ingredientes', this.ingredientes);
  }

  guardarEdit(form, modal) {
    let count = 1;
    let data = {
      id: form.id,
      nombre: form.nombre,
      valor: form.valor,
      descuento: form.descuento
    };
    this.tipeProductService.editarFormato(data).subscribe((resp: any) => {
      if (resp.code == 200) {
        modal.dismiss();
        if (this.formatosList.length == this.ingredientes.length && this.listRemove.length == 0) {
          console.log('INGRESAMOS AL PRIMER CASO NO ELIMINA Y MODIFICA');
          for (let i = 0; i < this.ingredientes.length; i++) {
            let ingrediente = {
              anterior_id: this.formatosList[i]['id'],
              ingrediente_id: this.ingredientes.value[i].id,
              cantidad: this.ingredientes.value[i].cantidad,
              formato_id: form.id
            };
            this.tipeProductService.editarIngredienteFormato(ingrediente).subscribe((resp: any) => {
              if (resp.code != 200) {
                console.log('se ingreso');
                this.Toast.fire({
                  icon: 'error',
                  title: resp.message
                });
              } 
            })
          }
        } else if (this.formatosList.length <= this.ingredientes.length && this.listRemove.length >= 0) {
          console.log('INGRESAMOS AL SEGUNDO CASO SE ELIMINA O NO Y SE AGREGAN');
          for (let i = 0; i < this.ingredientes.length; i++) {
            if (this.formatosList.length >= count) {
              if (this.ingredientes.value[i].id != this.formatosList[i]['id'] ||
                this.ingredientes.value[i].cantidad != this.formatosList[i]['cantidad']) {
                console.log('ingresamos :' + i);
                let ingrediente = {
                  anterior_id: this.formatosList[i]['id'],
                  ingrediente_id: this.ingredientes.value[i].id,
                  cantidad: this.ingredientes.value[i].cantidad,
                  formato_id: form.id
                };
                this.tipeProductService.editarIngredienteFormato(ingrediente).subscribe((resp1: any) => {
                  if (resp1.code != 200) {
                    this.Toast.fire({
                      icon: 'error',
                      title: resp.message
                    });
                  }
                })
              }
            } else if (this.formatosList[i] == undefined) {
              let ingrediente = {
                ingrediente_id: this.ingredientes.value[i].id,
                cantidad: this.ingredientes.value[i].cantidad,
                formato_id: form.id
              };
              this.tipeProductService.crearIngredienteFormato(ingrediente).subscribe((resp2: any) => {
                console.log('ingresamos a crear:' + i);
                if (resp2.code != 200) {
                  this.Toast.fire({
                    icon: 'error',
                    title: resp.message
                  });
                }
              })
            }
            count++;
          }
          for (let i = 0; i < this.listRemove.length; i++) {
            let data = {
              formato_id: form.id,
              ingrediente_id: this.listRemove[i].id
            };
            console.log(data);
            this.tipeProductService.eliminarIngredienteFormato(data).subscribe((resp3: any) => {
              if (resp3.code != 200) {
                this.Toast.fire({
                  icon: 'error',
                  title: resp.message
                });
              }
            })
          }
        } else if (this.formatosList.length > this.ingredientes.length && this.listRemove.length > 0) {
          console.log('INGRESAMOS AL TERCER CASO SE ELIMINA');
          for (let i = 0; i < this.ingredientes.length; i++) {
            if (this.ingredientes.length >= count) {
              if (this.ingredientes.value[i].id != this.formatosList[i]['id'] ||
                this.ingredientes.value[i].cantidad != this.formatosList[i]['cantidad']) {
                let dataDetail = {
                  anterior_id: this.formatosList[i]['id'],
                  ingrediente_id: this.ingredientes.value[i].id,
                  cantidad: this.ingredientes.value[i].cantidad,
                  formato_id: form.id
                };
                this.tipeProductService.editarIngredienteFormato(dataDetail).subscribe((resp4: any) => {
                  if (resp4.code != 200) {
                    this.Toast.fire({
                      icon: 'error',
                      title: resp.message
                    });
                  }
                })
              }
            } else if (this.formatosList[i] == undefined) {
              let dataDetail = {
                ingrediente_id: this.ingredientes.value[i].id,
                cantidad: this.ingredientes.value[i].cantidad,
                formato_id: form.id
              };
              this.tipeProductService.crearIngredienteFormato(dataDetail).subscribe((resp5: any) => {
                if (resp5.code != 200) {
                  this.Toast.fire({
                    icon: 'error',
                    title: resp.message
                  });
                }
              })
            }
            count++;
          }
          for (let i = 0; i < this.listRemove.length; i++) {
            console.log(this.listRemove[i]);
            let data = {
              formato_id: form.id,
              ingrediente_id: this.listRemove[i].id
            };
            console.log(data);
            this.tipeProductService.eliminarIngredienteFormato(data).subscribe((resp6: any) => {
              if (resp6.code != 200) {
                this.Toast.fire({
                  icon: 'error',
                  title: resp.message
                });
              }
            })
          }
        }
        this.obtenerListado(this.param.id);
        this.obtenerListadoTipoIngrediente();
        this.obtenerListadoTipoProducto();
        this.ingredientes.controls.splice(0, this.ingredientes.length);
        this.listRemove = [];
        this.productEdit.reset();
        this.Toast.fire({
          icon: 'success',
          title: resp.message
        });
      } else {
        this.Toast.fire({
          icon: 'error',
          title: resp.message
        });
      }
    });
  }

  eliminarFormato(id){
    let data = {
      id : id
    }
    console.log(data);
    this.tipeProductService.obtenerPedidosPorFormato(data).subscribe((resp:any)=>{
      console.log('resp: ', resp);
      if(resp.code === 200){
        this.Toast.fire({
          icon: 'error',
          title: resp.message
        });
      }else{
        Swal.fire({
          title: 'Estas seguro de querer eliminar el ingrediente?',
          text: "¡No podrás revertir esto!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirmar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.isConfirmed) {
            this.tipeProductService.eliminarIngredientesPorFormato(data).subscribe((resp1:any)=>{
              if(resp1.code === 200){
                this.tipeProductService.eliminarFormato(data).subscribe((resp2:any)=>{
                  if(resp2.code === 200){
                    this.obtenerListado(this.param.id)
                  }
                })
              }else if(resp1.code === 0){
                this.tipeProductService.eliminarFormato(data).subscribe((resp2:any)=>{
                  if(resp2.code === 200){
                    this.obtenerListado(this.param.id)
                  }
                })
              }
            })
          }
        })
      }
    })
  }

  dismiss(modal) {
    modal.dismiss();
    this.productCreate.reset();
    this.listRemove = [];
  }

}
