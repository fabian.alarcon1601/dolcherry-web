import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BrandService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoMarcas() {
    return this.http.get(url + '/marca/listar')
  }

  crearMarca(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/marca/crear', data, option);
  }

  eliminarMarca(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/marca/eliminar', data, option);
  }
  
  buscarIngredienteByMarca(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/marca/buscar/ingrediente', data, option);
  }

  editarMarca(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/marca/editar', data, option);
  }
}
