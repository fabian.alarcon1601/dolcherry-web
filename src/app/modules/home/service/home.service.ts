import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoInicio() {
    return this.http.get(url + '/inicio/listar')
  }

  listarCategorias() {
    return this.http.get(url + '/categoria/listar')
  }

  listarProductos() {
    return this.http.get(url + '/tipo/producto/listar/activos')
  }
  
  obtenerListadoFooter() {
    return this.http.get(url + '/footer/listar')
  }

  enviarContacto(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/contacto/enviar/correo', data, option);
  }

  obtenerFormatosPorProducto(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/buscar/formato', data, option);
  }
}
