import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-reset-pass',
  templateUrl: './reset-pass.component.html',
  styleUrls: ['./reset-pass.component.scss']
})
export class ResetPassComponent implements OnInit {
  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })


  correoUser = this.formBuilder.group({
    correo: new FormControl('', [Validators.required ,Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])
  });

  constructor( private router: Router, private formBuilder: FormBuilder, private userService : UserService) { }

  ngOnInit(): void {
  }

  login(){
    this.router.navigate(['/user-pages/login']);
  }

  solicitarContrasenia(item){
    console.log('item: ', item);
    let data = {
      correo: item.correo
    }
    this.userService.getCor(data).subscribe((resp:any)=>{
      if(resp.code === 200){
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.correoUser.reset()
        this.router.navigate(['/user-pages/login'])
      }else if(resp.code === 400){
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

}
