import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { AdminService } from '../../service/admin.service';

@Component({
  selector: 'app-edit-about',
  templateUrl: './edit-about.component.html',
  styleUrls: ['./edit-about.component.scss']
})
export class EditAboutComponent implements OnInit {
  isEdit = false;
  mensaje:any;
  datosEdit:any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  editAboutForm = this.formBuilder.group({
    mision: new FormControl({ value: null, disabled: !this.isEdit }, [Validators.required]),
    vision: new FormControl({ value: null, disabled: !this.isEdit }, [Validators.required])
  });


  constructor(private adminService: AdminService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.obtenerListado();
  }

  editAbout(value){
    this.isEdit = true;
    const mision = this.editAboutForm.get('mision');
    const vision = this.editAboutForm.get('vision');
   
    mision.enable();
    vision.enable();
  }

  saveAbout(values){
    console.log(values);
    let data = {
      mision: values.mision,
      vision: values.vision,
    }
    console.log(data);
    this.adminService.editarAbout(data).subscribe((resp: any) => {
      this.mensaje = resp.message
      if (resp.code === 200) {
        this.isEdit = false;
        const mision = this.editAboutForm.get('mision');
        const vision = this.editAboutForm.get('vision');

        mision.disable();
        vision.disable();
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado()
      } else {
        if (resp.code != 200) {
          (this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          }))
        }
      }
    })
  }
  
  obtenerListado(){
    this.adminService.obtenerListadoAbout().subscribe((resp: any) => {
      if (resp.code === 200) {
        this.datosEdit = resp.data;
        this.editAboutForm.patchValue(this.datosEdit)
      }
    })
  }
  
  cancel(){
    this.isEdit = false
    this.editAboutForm.reset()
  
    const mision = this.editAboutForm.get('mision');
    const vision = this.editAboutForm.get('vision');
   
    mision.disable();
    vision.disable();

    this.obtenerListado()
  }

  crearAbout(values){
    let data = {
      mision: values.mision,
      vision: values.vision,
    }
    console.log(data);
    this.adminService.crearAbout(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.isEdit = false;
        const mision = this.editAboutForm.get('mision');
        const vision = this.editAboutForm.get('vision');
        mision.disable();
        vision.disable();
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado()
      } else {
        if (resp.code != 200) {
          (this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          }))
        }
      }
    })
  }
}



