import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { RoleService } from '../../services/role.service';
import { RoleModel } from 'src/models/role.model'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  ItemRoles: any;
  RolCrear: any;
  RolEditar: any;
  RolActivar: any;
  RolDesactivar: any;
  roleModel: RoleModel;
  modalOptions2: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })


  rolCreate = this.formBuilder.group({
    nombre: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required)
  });

  constructor(private roleService: RoleService, private modalService: NgbModal, private formBuilder: FormBuilder) { 
    this.modalOptions2 = {
      backdrop:'static',
      size: 'lg'
    }
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado()
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.roleService.obtenerListadoRoles().subscribe((resp: any) => {
      this.ItemRoles = resp.data;
      this.rerender()
      this.rolCreate.reset();
    })

  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  editarRol(item: RoleModel) {
    this.roleModel = JSON.parse(JSON.stringify(item));
  }

  guardarEdit(modal) {
    this.roleService.editarRol(this.roleModel).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado()
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  activarRol(id) {
    let data = {
      id: id
    }
    this.roleService.activarRol(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.obtenerListado()
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  desactivarRol(id) {
    let data = {
      id: id
    }
    this.roleService.desactivarRol(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.obtenerListado()
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal){
    modal.dismiss();
    this.rolCreate.reset();
  }
}
