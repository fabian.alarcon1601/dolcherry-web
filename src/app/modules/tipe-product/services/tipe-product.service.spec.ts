import { TestBed } from '@angular/core/testing';

import { TipeProductService } from './tipe-product.service';

describe('TipeProductService', () => {
  let service: TipeProductService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipeProductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
