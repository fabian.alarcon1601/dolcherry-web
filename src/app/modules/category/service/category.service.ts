import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoCategorias() {
    return this.http.get(url + '/categoria/listar')
  }

  crearCategoria(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/categoria/crear', data, option);
  }

  buscarProductosPorCategoria(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/categoria/buscar/producto', data, option);
  }

  eliminarCategoria(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/categoria/eliminar',data, option);
  }

  editarCategoria(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/categoria/editar', data, option);
  }
}
