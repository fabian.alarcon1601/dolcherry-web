import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userLocal: any;
  isEdit1 = false;
  isEdit2 = false;
  mensaje: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })


  usuarioDates = this.formBuilder.group({
    nombre: new FormControl({ value: null, disabled: !this.isEdit1 }, [Validators.required]),
    apellidoPaterno: new FormControl({ value: null, disabled: !this.isEdit1 }, [Validators.required]),
    apellidoMaterno: new FormControl({ value: null, disabled: !this.isEdit1 }, [Validators.required]),
    telefono: new FormControl({ value: null, disabled: !this.isEdit1 }, [Validators.required, Validators.pattern('[- +()0-9]+')]),
  });

  changePassword = this.formBuilder.group({
    actualPass: new FormControl({ value: null, disabled: !this.isEdit2 }, [Validators.required]),
    newPass: new FormControl({ value: null, disabled: !this.isEdit2 }, [Validators.required]),
    newRePass: new FormControl({ value: null, disabled: !this.isEdit2 }, [Validators.required])
  })

  constructor(private formBuilder: FormBuilder, private profileService: ProfileService) {
  }

  ngOnInit(): void {
    this.obtenerUsuario();
  }

  asignarValoresForm(data) {
    this.usuarioDates.patchValue({ 'nombre': data.nombre })
    this.usuarioDates.patchValue({ 'apellidoPaterno': data.apellido_paterno })
    this.usuarioDates.patchValue({ 'apellidoMaterno': data.apellido_materno })
    this.usuarioDates.patchValue({ 'telefono': data.telefono })
  }

  obtenerUsuario() {
    let user = this.profileService.getUser();
    this.userLocal = user;
    let data = {
      id: user.id
    }
    this.profileService.getTelefonoUsuario(data).subscribe((res: any) => {
      this.asignarValoresForm(res.data);
    })
  }

  editUsuario(values) {
    this.isEdit1 = true;
    const nombre = this.usuarioDates.get('nombre');
    const apellidoPaterno = this.usuarioDates.get('apellidoPaterno');
    const apellidoMaterno = this.usuarioDates.get('apellidoMaterno');
    const telefono = this.usuarioDates.get('telefono');

    nombre.enable();
    apellidoPaterno.enable();
    apellidoMaterno.enable();
    telefono.enable();
  }

  saveUsuario(values) {
    let data = {
      id: this.userLocal.id,
      nombre: values.nombre,
      apellido_paterno: values.apellidoPaterno,
      apellido_materno: values.apellidoMaterno,
      telefono: values.telefono,
    }
    console.log(data);
    this.profileService.editarPerfil(data).subscribe((resp: any) => {
      this.mensaje = resp.message
      if (resp.code === 200) {
        this.isEdit1 = false;
        const nombre = this.usuarioDates.get('nombre');
        const apellidoPaterno = this.usuarioDates.get('apellidoPaterno');
        const apellidoMaterno = this.usuarioDates.get('apellidoMaterno');
        const telefono = this.usuarioDates.get('telefono');
        nombre.disable();
        apellidoPaterno.disable();
        apellidoMaterno.disable();
        telefono.disable();
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerUsuario()
      } else {
        if (resp.code != 200) {
          (this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          }))
        }
      }
    })
    this.Toast.fire({
      icon: 'error',
      title: `${this.mensaje}`
    })
  }

  cancelState() {
    this.usuarioDates.reset();
    this.cambioTab2()
    this.obtenerUsuario();
  }

  cambioTab2() {
    this.isEdit1 = false;
    const nombre = this.usuarioDates.get('nombre');
    const apellidoPaterno = this.usuarioDates.get('apellidoPaterno');
    const apellidoMaterno = this.usuarioDates.get('apellidoMaterno');
    const telefono = this.usuarioDates.get('telefono');
    nombre.disable();
    apellidoPaterno.disable();
    apellidoMaterno.disable();
    telefono.disable();
  }


  // 
  // ------------------------------------------------------------------------------
  // 


  resetMisDatos($event: NgbTabChangeEvent) {
    console.log('mis datos ');
    if ($event.nextId === 'tab-2') {
      this.isEdit1 = false;
      this.usuarioDates.reset()
      console.log('is tab-2');
      this.cambioTab2();
    }
    if ($event.nextId === 'tab-1') {
      this.isEdit2 = false;
      this.obtenerUsuario()
      console.log('is tab-1');
      this.cambioTab1();
    }
  }


  // 
  // -----------------------------------------------------------------------------
  // 


  changePass() {
    this.isEdit2 = true;
    const actualPass = this.changePassword.get('actualPass');
    const newPass = this.changePassword.get('newPass');
    const newRePass = this.changePassword.get('newRePass');

    actualPass.enable();
    newPass.enable();
    newRePass.enable();
  }

  cambioTab1() {
    this.isEdit2 = false;
    const actualPass = this.changePassword.get('actualPass');
    const newPass = this.changePassword.get('newPass');
    const newRePass = this.changePassword.get('newRePass');

    actualPass.disable();
    newPass.disable();
    newRePass.disable();
  }

  cancelState2() {
    this.usuarioDates.reset();
    this.cambioTab1();
  }

  cambiarContrasenia(form){
    let data = {
      id: this.userLocal.id,
      actual: form.actualPass,
      newPassword : form.newPass,
      newRePassword : form.newRePass
    }
    console.log(data);
    this.profileService.cambiarContrasenia(data).subscribe((resp : any) => {
      if(resp.code === 200){
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.isEdit2 = false;
        this.changePassword.reset();
        this.cambioTab1();
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }
}
