import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../service/home.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  homeList: any;
  imagen:any;

  constructor(private homeService : HomeService ) { }

  ngOnInit(): void {
    this.listHome();
  }

  listHome(){
    this.homeService.obtenerListadoInicio().subscribe((resp: any) => {
      if(resp.code === 200){
        this.homeList = resp.data;
        this.imagen = this.obtenerImagen(resp.data.imagen)
      }else{
        let data = {
          titulo: 'Dolcherry',
          subtitulo: 'Patisserie',
        }
        this.homeList= data
        this.imagen = 'http://localhost:3001/Images/home/1659213454340.jpeg'
      }
      
    })
  }

  obtenerImagen(item){
    let resultado = item.replaceAll( '\\', "/")
    let ruta = 'http://localhost:3001/'
    return ruta+resultado;
  }
}
