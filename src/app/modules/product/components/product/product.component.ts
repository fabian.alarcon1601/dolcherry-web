import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { FormatModel } from 'src/models/format.model';
import Swal from 'sweetalert2';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit , OnDestroy, AfterViewInit {
    @ViewChild(DataTableDirective, { static: false })
    dtElement: DataTableDirective;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject<any>();
    productModel: FormatModel;
    itemProducts : any;
    ids = [];
    categorias: any;
    tipoProducto:any;
    tipoIngrediente: any;
    ingredientArray = []
  
    Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
 
    productCreate = this.formBuilder.group({
      producto_id: new FormControl('', Validators.required),
      nombre: new FormControl('', Validators.required),
      valor: new FormControl('', Validators.required),
      ingredientes : this.formBuilder.array([])
    });

    
    constructor(private productService: ProductService, private modalService: NgbModal, private formBuilder: FormBuilder) { }
    
    
    // --------------------------------------------------------------------------------------------

    // 
    // ingredientes
    // 

    get ingredientes() {
      return this.productCreate.controls["ingredientes"] as FormArray;
    }
  
    addIngrediente() {
      const ingredientFormGroup = this.formBuilder.group({
        id: new FormControl(''),
        cantidad: new FormControl(''),
      });
      this.ingredientes.push(ingredientFormGroup);
      console.log(this.ingredientes);
    }
  
    removeIngrediente(i: number) {
      if (this.ingredientes.value[i] != undefined) {
        this.ids.push(this.ingredientes.value[i].id);
      }
      this.ingredientes.removeAt(i);
    }

    // 
    // ingredientes
    // 

    ngAfterViewInit(): void {
      this.dtTrigger.next();
    }
  
    ngOnDestroy(): void {
      this.dtTrigger.unsubscribe()
    }
  
    ngOnInit(): void {
      this.dtOptions = {
        language: Language.spanish_datatables,
        responsive: true,
        pagingType: 'full_numbers',
        pageLength: 7,
        destroy: true,
        retrieve: true
      };
      this.obtenerListado();
      this.obtenerListadoTipoIngrediente();
      this.obtenerListadoTipoProducto();
    }
  
    rerender(): void {
      if ("dtInstance" in this.dtElement) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }
      else {
        this.dtTrigger.next();
      }
    }
  
    obtenerListado() {
      this.productService.obtenerListadoProductos().subscribe((resp: any) => {
        console.log(resp);
        this.itemProducts = resp.data;
        this.rerender();
      })
    }

    obtenerListadoTipoProducto() {
      this.productService.obtenerListadoTipoProducto().subscribe((resp: any) => {
        this.tipoProducto = resp.data;
      })
    }

    obtenerListadoTipoIngrediente() {
      this.productService.obtenerListadoTiposIngredientes().subscribe((resp: any) => {
        this.tipoIngrediente = resp.data;
      })
    }
  
    openModal(content) {
      this.modalService.open(content, { size: 'lg' });
    }
  
    crearProducto(form, modal) {
      console.log(form);
      if (form.nombre != "" && form.valor != "" && form.categoria_id != "" && form.tipo_producto_id != "") {
        let data = {
          nombre: form.nombre,
          valor: form.valor,
          producto_id: form.producto_id,
          ingredientes: form.ingredientes
        }
        console.log(data);
        this.productService.crearProducto(data).subscribe((resp: any) => {
          if (resp.code === 200) {
            this.Toast.fire({
              icon: 'success',
              title: `${resp.message}`
            })
            modal.dismiss()
            this.obtenerListado()
            this.productCreate.reset()
          }else{
            this.Toast.fire({
              icon: 'error',
              title: `${resp.message}`
            })
          }
        })
      }else{
        this.Toast.fire({
          icon: 'error',
          title: `Ingrese todos los datos`
        })
      }
    }
  
    editarProducto(item: FormatModel) {
      this.productModel = JSON.parse(JSON.stringify(item));
    }
  
    // eliminarProducto(id){
    //   let data = {
    //     id : id
    //   }
    //   this.productCreate.buscarProductosPorCategoria(data).subscribe((resp: any) =>{
    //     if(resp.code === 200){
    //       this.Toast.fire({
    //         icon: 'error',
    //         title: `${resp.message}`
    //       })
    //     }else{
    //       if(resp.code === 400){
    //         this.productCreate.eliminarCategoria(data).subscribe((resp: any) =>{
    //           if(resp.code === 200) {
    //             this.Toast.fire({
    //               icon: 'success',
    //               title: `${resp.message}`
    //             })
    //             this.obtenerListado();
    //           }else{
    //             this.Toast.fire({
    //               icon: 'error',
    //               title: `${resp.message}`
    //             })
    //           }
    //         })
    //       }
    //     }
    //   })
    // }
  
    guardarEdit(modal){
      this.productService.editarProducto(this.productModel).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss();
          this.obtenerListado()
        }else{
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    }
  
    dismiss(modal){
      modal.dismiss();
      this.productCreate.reset();
    }
  
  }
  
