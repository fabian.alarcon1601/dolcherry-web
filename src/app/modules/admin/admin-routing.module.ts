import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { EditFooterComponent } from './components/edit-footer/edit-footer.component';
import { EditHomeComponent } from './components/edit-home/edit-home.component';
import { EditAboutComponent } from './components/edit-about/edit-about.component';

const routes: Routes = [
  { path: 'home', component: EditHomeComponent},
  { path: 'users', component: UserComponent},
  { path: 'footer', component: EditFooterComponent},
  { path: 'about', component: EditAboutComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class AdminRoutingModule { }
