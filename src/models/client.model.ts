export class ClientModel{
  //Se modela segun base de datos.
  id:number;
  nombre_completo:string;
  correo:string;
  direccion:string;
  telefono:string;
}