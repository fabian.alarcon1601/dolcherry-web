export class IngredientModel{
  //Se modela segun base de datos.
  id:number;
  nombre:string;
  marca_id:number;
  unidad_medida:string;
  stock_minimo:number;
}