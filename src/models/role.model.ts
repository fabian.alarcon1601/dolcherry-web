export class RoleModel{
    //Se modela segun base de datos.
    id:number;
    nombre:string;
    operativo:boolean;
    descripcion:string;
}