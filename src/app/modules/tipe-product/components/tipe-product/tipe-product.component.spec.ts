import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipeProductComponent } from './tipe-product.component';

describe('TipeProductComponent', () => {
  let component: TipeProductComponent;
  let fixture: ComponentFixture<TipeProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipeProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipeProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
