import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoClientes() {
    return this.http.get(url + '/cliente/listar')
  }

  crearCliente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/cliente/crear', data, option);
  }

  editarCliente(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/cliente/editar', data, option);
  }

  pedidosCliente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/cliente/pedidos', data, option);
  }

  eliminarCliente(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/cliente/eliminar', data, option);
  }
}
