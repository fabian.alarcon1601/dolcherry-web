import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ProductValidator } from '../../validations';
import { Language } from 'src/app/translateDataTable/language';
import { ProductModel } from 'src/models/product.model';
import Swal from 'sweetalert2';
import { fileURLToPath } from 'url';
import { TipeProductService } from '../../services/tipe-product.service';

@Component({
  selector: 'app-tipe-product',
  templateUrl: './tipe-product.component.html',
  styleUrls: ['./tipe-product.component.scss']
})
export class TipeProductComponent implements OnInit, AfterViewInit, OnDestroy {
  public archivos: any = [];
  public previsualizacion: string;

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  productModel: ProductModel;
  itemProducts: any;
  ids = [];
  imagenProducto: any;
  categorias: any;
  tipoProducto: any;
  tipoIngrediente: any;
  ingredientArray = []
  modalOptions2: any;
  imagenEdit: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  productCreate = this.formBuilder.group({
    imagen: new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
    categoria_id: new FormControl('', [Validators.required , ProductValidator.verificarSelect]),
    descripcion: new FormControl('', Validators.required)
  });

  productEdit = this.formBuilder.group({
    id: new FormControl('', Validators.required),
    imagen: new FormControl(''),
    nombre: new FormControl('', Validators.required),
    categoria_id: new FormControl('', [Validators.required , ProductValidator.verificarSelect]),
    descripcion: new FormControl('', Validators.required)
  });

  @ViewChild('imagen') fileinput: ElementRef;
  constructor(private sanitizer: DomSanitizer, private tipeProductService: TipeProductService, private modalService: NgbModal, private formBuilder: FormBuilder, private router: Router) {
    this.modalOptions2 = {
      backdrop: 'static',
      size: 'lg'
    }
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado();
    this.obtenerListadoCategorias();
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  navigate(item) {
    let format = item.nombre.replaceAll(' ', '-').toLowerCase();
    this.router.navigate(['tipe-product/formato', format], {
      state: {
        item
      }
    });
  }

  obtenerListado() {
    this.tipeProductService.obtenerListadoProductos().subscribe((resp: any) => {
      this.itemProducts = resp.data;
      this.rerender();
    })
  }


  obtenerListadoCategorias() {
    this.tipeProductService.obtenerListadoCategorias().subscribe((resp: any) => {
      this.categorias = resp.data;
      this.rerender();
    })
  }

  openModalCrear(content) {
    this.productCreate.reset();
    this.previsualizacion = '';
    this.modalService.open(content, this.modalOptions2);
  }

  openModalEditar(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  // captura de imagen
  // ----------------------------------------------------------------------------------------------

  obtenerImagen(item) {
    let resultado = item.replace('\\', "/")
    let ruta = 'http://localhost:3001/'
    return ruta + resultado;
  }

  capturarFile(event) {
    const archivoCapturado = event.target.files[0]
    this.extraerBase64(archivoCapturado).then((imagen: any) => {
      this.previsualizacion = imagen.base;
    })
    this.archivos.push(archivoCapturado);
  }

  fileChange(file: any) {
    this.imagenProducto = file[0]
  }

  fileChangeCreate(file: any) {
    this.imagenProducto = file[0]
  }

  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          base: null
        });
      };
    } catch (e) {
      return null;
    }
  })

  // captura de imagen
  // ----------------------------------------------------------------------------------------------

  crearProducto(form, modal) {
    if (form.nombre != "" && form.valor != "" && form.categoria_id != "") {
      let formdata = new FormData();
      formdata.append('nombre', form.nombre)
      formdata.append('imagen', this.imagenProducto)
      formdata.append('descripcion', form.descripcion)
      formdata.append('categoria_id', form.categoria_id)
      this.tipeProductService.crearProducto(formdata).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss()
          this.obtenerListado()
          this.productCreate.reset()
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos`
      })
    }
  }

  activar(id) {
    let data = {
      id: id
    }
    this.tipeProductService.activarProducto(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.obtenerListado()
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
      } else {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  desactivar(id) {
    let data = {
      id: id
    }
    this.tipeProductService.desactivarProducto(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.obtenerListado()
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
      } else {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  editarProducto(value) {
    this.productEdit.reset();
    this.previsualizacion = '';
    this.productEdit.patchValue({ 'id': value.id });
    this.productEdit.patchValue({ 'nombre': value.nombre });
    this.productEdit.patchValue({ 'categoria_id': value.categoria_id });
    this.productEdit.patchValue({ 'descripcion': value.descripcion });
    this.imagenEdit = this.obtenerImagen(value.imagen)

  }

  guardarEdit(form, modal) {
    let data = {
      id: form.id,
      nombre: form.nombre,
      categoria_id: form.categoria_id,
      descripcion: form.descripcion
    }
    this.tipeProductService.editarProducto(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        if (form.imagen != null) {
          let formdata = new FormData();   
          formdata.append('id', form.id);       
          formdata.append('imagen', this.imagenProducto)
          this.tipeProductService.editarImagenProducto(formdata).subscribe((resp: any) => {
            if(resp.code === 200){
              this.Toast.fire({
                icon: 'success',
                title: `${resp.message}`
              })
              modal.dismiss();
              this.obtenerListado()
            }else{
              this.Toast.fire({
                icon: 'error',
                title: `${resp.message}`
              })
            }
          })
        } else {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss();
          this.obtenerListado()
        }
      } else {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  valorizarInventario() {
    this.tipeProductService.valorizarIngredientes().subscribe((resp: any) => {
      if (resp.code === 200) {
        this.tipeProductService.valorizarFormatosProductos().subscribe((resp: any) => {
          Swal.close()
        })
      }
    })
    let timerInterval
    Swal.fire({
      title: 'Valorizando inventario',
      html: 'Espere un momento por favor ',
      backdrop: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading()
        const b = Swal.getHtmlContainer().querySelector('b')
      },
      willClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        this.obtenerListado()
      }
    })
  }

  eliminarProducto(id) {
    let data = {
      id: id
    }
    this.tipeProductService.obtenerFormatosPorProducto(data).subscribe((resp: any) => {
      console.log(resp);
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      } else {
        if (resp.code === 400) {
          Swal.fire({
            title: '¿Estas seguro?',
            text: "¡No podrás revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Confirmar'
          }).then((result) => {
            if (result.isConfirmed) {
              this.tipeProductService.eliminarProducto(data).subscribe((resp: any) => {
                if (resp.code === 200) {
                  this.Toast.fire({
                    icon: 'success',
                    title: `${resp.message}`
                  })
                  this.obtenerListado();
                } else {
                  this.Toast.fire({
                    icon: 'error',
                    title: `${resp.message}`
                  })
                }
              })
            }
          })
        }
      }
    })
  }

  dismiss(modal) {
    modal.dismiss();
    this.productCreate.reset();
    this.productEdit.reset();
    this.previsualizacion = '';
  }

}


