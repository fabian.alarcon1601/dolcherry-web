import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  helper = new JwtHelperService()
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  editarUsuario(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/footer/editar', data, option);
  }

  editarPerfil(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/usuario/editar/perfil', data, option);
  }

  getTelefonoUsuario(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/usuario/buscar/id', data, option);
  }

  getUser(){
    let {usuario} = this.helper.decodeToken(localStorage.getItem('token'));
    return usuario;
  }

  cambiarContrasenia(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/usuario/change/password', data, option);
  }
}
