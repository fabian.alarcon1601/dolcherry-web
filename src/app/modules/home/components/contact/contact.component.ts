import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { HomeService } from '../../service/home.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  google:any;
  homeList: any;
  imagen: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  contactForm = this.formBuilder.group({
    nombre: new FormControl('', Validators.required),
    correo: new FormControl('',[Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    telefono: new FormControl('',[Validators.required, Validators.pattern('[- +()0-9]+') ,Validators.minLength(9), Validators.maxLength(9)]),
    asunto: new FormControl('', Validators.required),
    mensaje: new FormControl('', Validators.required)
  });

  constructor(private homeService: HomeService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.listHome()
  }

  listHome(){
    this.homeService.obtenerListadoInicio().subscribe((resp: any) => {
      if(resp.code === 200){
        this.homeList = resp.data;
        this.imagen = this.obtenerImagen(resp.data.imagen)
      }else{
        this.imagen = 'http://localhost:3001/Images/home/1659213454340.jpeg'
      }
      
    })
  }

  obtenerImagen(item){
    let resultado = item.replaceAll( '\\', "/")
    let ruta = 'http://localhost:3001/'
    return ruta+resultado;
  }

  enviarContacto(form){
    let data ={
      nombre: form.nombre,
      telefono: form.telefono,
      correo: form.correo,
      asunto: form.asunto,
      mensaje: form.mensaje,
    }
    this.homeService.enviarContacto(data).subscribe((resp: any) =>{
      if (resp.code == 200){
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.contactForm.reset();
      }
    })
  }
}
