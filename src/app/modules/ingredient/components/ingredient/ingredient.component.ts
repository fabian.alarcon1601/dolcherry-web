import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { StockModel } from 'src/models/stock.model';
import { IngredientService } from '../../service/ingredient.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  ingredients: any;
  tipeIngredient: any;
  brands: any;
  ingredientModel: StockModel;
  modalOptions2: any;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  ingredientCreate = this.formBuilder.group({
    unidad_medida: new FormControl('', Validators.required),
    cantidad: new FormControl('', Validators.required),
    ingrediente_id: new FormControl('', Validators.required),
    codigo: new FormControl('', Validators.required),
    fecha_caducidad: new FormControl('', Validators.required),
    precio_ingreso: new FormControl('', Validators.required)
  });


  constructor(private ingredientService: IngredientService, private modalService: NgbModal, private formBuilder: FormBuilder) {
    this.modalOptions2 = {
      backdrop: 'static',
      size: 'lg'
    }
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado()
    this.obtenerListadoMarcas();
    this.obtenerListadoTipoIngrediente();
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.ingredientService.obtenerListadoIngredientes().subscribe((resp: any) => {
      console.log(resp.data)
      this.ingredients = resp.data;
      this.rerender()
    })
  }

  obtenerListadoTipoIngrediente() {
    this.ingredientService.obtenerListadoTipoIngrediente().subscribe((resp: any) => {
      console.log(resp);
      this.tipeIngredient = resp.data;
    })
  }

  obtenerListadoMarcas() {
    this.ingredientService.obtenerListadoMarcas().subscribe((resp: any) => {
      this.brands = resp.data;
    })
  }

  openModal(content) {
    this.modalService.open(content,this.modalOptions2);

  }

  openModalEdit(content) {
    this.modalService.open(content,this.modalOptions2);
  }

  crearIngrediente(form, modal) {
    if (form.unidad_medida != "" && form.peso_total != "" && form.tipo_ingrediente != "" && form.codigo != "" && form.fecha_caducidad != "") {
      let data = {
        unidad_medida: form.unidad_medida,
        peso_total: form.cantidad,
        ingrediente_id: form.ingrediente_id,
        codigo: form.codigo,
        fecha_caducidad: form.fecha_caducidad,
        precio_ingreso: form.precio_ingreso
      }
      this.ingredientService.crearIngrediente(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          let data2 = {
            id: form.ingrediente_id,
            precio_valorizacion: form.precio_ingreso
          }
          this.ingredientService.actualizarIngrediente(data2).subscribe((resp2: any) => {
            if (resp2.code === 200) {
              console.log('precio cambiado');
            } else {
              console.log('no se puede cambiar el precio');
            }
          })
          modal.dismiss()
          this.obtenerListado()
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  editarIngrediente(item: StockModel) {
    this.ingredientModel = JSON.parse(JSON.stringify(item));
  }

  eliminarIngrediente(id) {
    let data = {
      id: id
    }
    this.ingredientService.eliminarIngrediente(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado();
      } else {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  guardarEdit(modal) {
    console.log(this.ingredientModel)
    this.ingredientService.editarIngrediente(this.ingredientModel).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado()
      } else {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal) {
    modal.dismiss();
    this.ingredientCreate.reset();
  }
}
