import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { OrderModel } from 'src/models/order.model';
import Swal from 'sweetalert2';
import { OrderService } from '../../service/order.service';

@Component({
  selector: 'app-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.scss']
})
export class TodayComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  itemOrderToday:any;
  detalleOrden:any;
  modalOptions: any;
  modalOptions2: any;
  datosCliente: any;
  asignaEntrega: OrderModel;

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  editarEstadoPedido = this.formBuilder.group({
    recibido_por: new FormControl('', Validators.required),
  });

  
  constructor(private modalService: NgbModal, private formBuilder: FormBuilder, private orderService: OrderService) {
    this.modalOptions = {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    }

    this.modalOptions2 = {
      backdrop: 'static',
      size: 'lg'
    }
   }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado()
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado() {
    this.orderService.obtenerListadoOrdenesHoy().subscribe((resp: any) => {
      console.log(resp.data)
      this.itemOrderToday = resp.data;
      this.rerender()
    })
  }
  
  dismiss(modal) {
    modal.dismiss();
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  openModalEntregar(content) {
    this.editarEstadoPedido.reset()
    this.modalService.open(content, this.modalOptions2);
  }

  openModal2(content) {
    this.modalService.open(content, this.modalOptions);
  }

  asignarPedido(item: OrderModel) {
    this.asignaEntrega = item;
  }

  entregarOrder(form, modal) {
    if (form.recibido_por != "") {
      let data = {
        id: this.asignaEntrega.id,
        recibido_por: form.recibido_por
      }
      this.orderService.actualizarRecibido(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          modal.dismiss();
          this.obtenerListado()
        }else{
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  verPedido(item) {
    this.datosCliente = item;
    let data = {
      id: item.id
    }
    this.orderService.obtenerFormatosPorPedido(data).subscribe((resp: any) => {
      console.log(resp.data);
      this.detalleOrden = resp.data.formatos;
    })
  }

  cancelarEntrega(item) {
    Swal.fire({
      title: 'Estas seguro de cancelar la entrega?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar'
    }).then((result) => {
      if (result.isConfirmed) {
        let data = {
          id: item.id
        }
        this.orderService.cancelarEntregaPedido(data).subscribe((resp: any) => {
          this.obtenerListado()
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
        })

      }
    })
  }
}
