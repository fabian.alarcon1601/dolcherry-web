import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipeProductService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoProductos() {
    return this.http.get(url + '/tipo/producto/listar')
  }

  obtenerListadoCategorias() {
    return this.http.get(url + '/categoria/listar')
  }

  obtenerListadoTipoProducto() {
    return this.http.get(url + '/tipo/producto/listar')
  }

  obtenerListadoTiposIngredientes() {
    return this.http.get(url + '/tipoIngrediente/listar')
  }

  crearProducto(data){
    let strContent = 'application/json';
    let header = new HttpHeaders().set('Accept', strContent)
    const option = {headers:header};
    return this.http.post(url+'/tipo/producto/crear', data, option);
  }

  editarImagenProducto(data){
    let strContent = 'application/json';
    let header = new HttpHeaders().set('Accept', strContent)
    const option = {headers:header};
    return this.http.post(url+'/tipo/producto/editar/imagen', data, option);
  }

  editarProducto(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/tipo/producto/editar', data, option);
  }

  obtenerFormatosPorProducto(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/buscar/formato', data, option);
  }

  obtenerIngredientesPorFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/listar/ingredientes', data, option);
  }

  crearFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/crear', data, option);
  }

  editarFormato(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/producto/editar', data, option);
  }
  
  activarProducto(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/tipo/producto/activar', data, option);
  }
  
  desactivarProducto(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/tipo/producto/desactivar', data, option);
  }
  
  valorizarIngredientes() {
    return this.http.get(url + '/tipoIngrediente/valorizar/ingrediente')
  }
  
  valorizarFormatosProductos() {
    return this.http.get(url + '/producto/valorizar/formatos')
  }
  
  editarIngredienteFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/editar/relacion', data, option);
  }
  
  eliminarIngredienteFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/formato/ingrediente/eliminar', data, option);
  }

  crearIngredienteFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/crear/relacion', data, option);
  }

  eliminarFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/producto/eliminar', data, option);
  }
  
  obtenerPedidosPorFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/formato/pedido/buscar/formatos', data, option);
  }

  eliminarIngredientesPorFormato(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/formato/ingrediente/eliminar/all', data, option);
  }

  eliminarProducto(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/tipo/producto/eliminar', data, option);
  }
}