export class StockModel{
  //Se modela segun base de datos.
  id:number;
  peso_total:number;
  peso_actual:number;
  codigo:string;
  fecha_caducidad:Date;
  precio_ingreso:number;
}