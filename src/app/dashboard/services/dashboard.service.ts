import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerPedidosMes() {
    return this.http.get(url + '/pedido/count/mes')
  }

  obtenerIngredientesVencidosMes() {
    return this.http.get(url + '/ingrediente/vencido')
  }

  obtenerIngredientesPorVencer() {
    return this.http.get(url + '/ingrediente/porvencer')
  }

  obtenerIngredientesVencidosMesAnterior() {
    return this.http.get(url + '/ingrediente/vencido/mesanterior')
  }

  obtenerPedidosDosMesesAnteriores() {
    return this.http.get(url + '/pedido/count/anteriores')
  }
}
