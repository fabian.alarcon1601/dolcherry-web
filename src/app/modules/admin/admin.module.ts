import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { EditFooterComponent } from './components/edit-footer/edit-footer.component';
import { EditHomeComponent } from './components/edit-home/edit-home.component';
import { UserComponent } from './components/user/user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { EditAboutComponent } from './components/edit-about/edit-about.component';


@NgModule({
  declarations: [UserComponent, EditFooterComponent, EditHomeComponent, EditAboutComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class AdminModule { }
