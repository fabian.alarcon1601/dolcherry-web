import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from './components/order/order.component';
import { TodayComponent } from './components/today/today.component';
import { TomorrowComponent } from './components/tomorrow/tomorrow.component';
import { AllorderComponent } from './components/allorder/allorder.component';

const routes: Routes = [
  {
    path: 'all',
    component: OrderComponent
  },
  {
    path: 'today',
    component: TodayComponent
  },
  {
    path: 'tomorrow',
    component: TomorrowComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
  
})
export class OrderRoutingModule { }
