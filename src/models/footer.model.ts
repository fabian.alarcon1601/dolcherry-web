export class FormatoModel{
  //Se modela segun base de datos.
  id:number;
  instagram:string;
  facebook:string;
  whatsapp:string;
  ubicacion_google:string;
  numero_celular:string;
  correo:string;
}