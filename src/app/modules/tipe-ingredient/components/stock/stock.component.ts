import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { ThemeService } from 'ng2-charts';
import { Subject } from 'rxjs';
import { Language } from 'src/app/translateDataTable/language';
import { StockModel } from 'src/models/stock.model';
import Swal from 'sweetalert2';
import { TipeIngredientService } from '../../service/tipe-ingredient.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  ingredients: [];
  tipeIngredient: any;
  param: any;
  ingredientModel: StockModel;
  modalOptions2: any;


  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  ingredientCreate = this.formBuilder.group({
    cantidad: new FormControl('', Validators.required),
    codigo: new FormControl('', Validators.required),
    fecha_caducidad: new FormControl('', Validators.required),
    precio_ingreso: new FormControl('', Validators.required)
  });


  constructor(private tipeIngredientService: TipeIngredientService, private modalService: NgbModal, private formBuilder: FormBuilder, private router: Router) {
    this.receiverParameters();
    this.modalOptions2 = {
      backdrop: 'static',
      size: 'lg'
    }
  }

  receiverParameters() {
    let para = this.router.getCurrentNavigation().extras.state; 
    if(para != undefined){
      this.param = para.item
    }else{
      this.router.navigateByUrl('/tipe-ingredient')
    }
  }
  
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.dtOptions = {
      language: Language.spanish_datatables,
      responsive: true,
      pagingType: 'full_numbers',
      pageLength: 7,
      destroy: true,
      retrieve: true
    };
    this.obtenerListado(this.param.id)
  }

  rerender(): void {
    if ("dtInstance" in this.dtElement) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
    else {
      this.dtTrigger.next();
    }
  }

  obtenerListado(item) {
    console.log(item);
    let data = {
      id: item
    }
    this.tipeIngredientService.buscarIngredienteStock(data).subscribe((resp: any) => {
      console.log(resp.data)
      this.ingredients = resp.data;
      this.rerender()
    })
  }

  openModal(content) {
    this.modalService.open(content, this.modalOptions2);

  }

  openModalEdit(content) {
    this.modalService.open(content, this.modalOptions2);
  }

  crearIngrediente(form, modal) {
    if (form.unidad_medida != "" && form.peso_total != "" && form.tipo_ingrediente != "" && form.codigo != "" && form.fecha_caducidad != "") {
      let data = {
        peso_total: form.cantidad,
        ingrediente_id: this.param.id,
        codigo: form.codigo,
        fecha_caducidad: form.fecha_caducidad,
        precio_ingreso: form.precio_ingreso
      }
      this.tipeIngredientService.crearIngrediente(data).subscribe((resp: any) => {
        if (resp.code === 200) {
          this.Toast.fire({
            icon: 'success',
            title: `${resp.message}`
          })
          let data2 = {
            id: this.param.id,
            stock_actual: form.cantidad / 1000,
            precio_valorizacion: form.precio_ingreso / form.cantidad
          }
          this.tipeIngredientService.actualizarIngrediente(data2).subscribe((resp2: any) => {
            if (resp2.code === 200) {
              console.log('precio cambiado');
            } else {
              console.log('no se puede cambiar el precio');
            }
          })
          this.ingredientCreate.reset();
          modal.dismiss()
          this.obtenerListado(this.param.id)
        } else {
          this.Toast.fire({
            icon: 'error',
            title: `${resp.message}`
          })
        }
      })
    } else {
      this.Toast.fire({
        icon: 'error',
        title: `Ingrese todos los datos requeridos`
      })
    }
  }

  editarIngrediente(item: StockModel) {
    this.ingredientModel = JSON.parse(JSON.stringify(item));
  }

  eliminarStock(item) {
    let data = {
      id: item.id
    }
    this.tipeIngredientService.eliminarIngrediente(data).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        this.obtenerListado(this.param.id);
      } else {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  guardarEdit(modal) {
    console.log(this.ingredientModel)
    this.tipeIngredientService.editarIngrediente(this.ingredientModel).subscribe((resp: any) => {
      if (resp.code === 200) {
        this.Toast.fire({
          icon: 'success',
          title: `${resp.message}`
        })
        modal.dismiss();
        this.obtenerListado(this.param.id)
      } else {
        this.Toast.fire({
          icon: 'error',
          title: `${resp.message}`
        })
      }
    })
  }

  dismiss(modal) {
    modal.dismiss();
    this.ingredientCreate.reset();
  }
}
