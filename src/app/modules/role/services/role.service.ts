import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private headers = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  );
  constructor(private http: HttpClient) { }

  obtenerListadoRoles() {
    return this.http.get(url + '/rol/listar')
  }

  crearRol(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/rol/crear', data, option);
  }

  editarRol(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/rol/editar', data, option);
  }

  activarRol(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/rol/activar', data, option);
  }

  desactivarRol(data){
    const option = {headers:this.headers};
    return this.http.put(url+'/rol/desactivar', data, option);
  }

  buscarUsuariosPorRol(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/rol/buscar/usuario', data, option);
  }
  
  eliminarRol(data){
    const option = {headers:this.headers};
    return this.http.post(url+'/rol/eliminar',data, option);
  }
}
